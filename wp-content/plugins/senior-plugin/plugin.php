<?php
/**
 * Plugin Name: Senior Plugin
 * Plugin URI:  http://wordpress.org/plugins
 * Description: Senior theme helper plugin
 * Version:     2.2.1
 * Author:      Aislin Themes
 * Author URI:  http://themeforest.net/user/Aislin/portfolio
 * License:     GPLv2+
 * Text Domain: chp
 * Domain Path: /languages
 */
define( 'SCP_PLUGIN_VERSION', '2.2.1' );
define( 'SCP_PLUGIN_NAME'   , 'Senior' );
define( 'SCP_PLUGIN_PREFIX' , 'scp_' );
define( 'SCP_PLUGIN_URL'    , plugin_dir_url( __FILE__ ) );
define( 'SCP_PLUGIN_PATH'   , dirname( __FILE__ ) . '/' );
define( 'SCP_TEXT_DOMAIN'   , 'scp_senior' );


register_activation_hook( __FILE__, 'scp_activate' );
register_deactivation_hook( __FILE__, 'scp_deactivate' );

add_action( 'plugins_loaded', 'scp_init' );
add_action( 'widgets_init', 'scp_register_wp_widgets' );
add_action( 'admin_init', 'scp_vc_editor_set_post_types', 11 );
add_action( 'wp_head', 'scp_set_js_global_var' );

// Dynamically add a section. Can be also used to modify sections/fields
// add_filter( 'redux/options/wheels_options/sections', 'scp_dynamic_section' );

add_filter( 'pre_get_posts', 'scp_portfolio_posts' );

require_once 'shortcodes.php';

function scp_clean($item) {
    $firstClosingPTag = substr($item, 0, 4);
    $lastOpeningPTag  = substr($item, -3);

    if ($firstClosingPTag == '</p>') {
        $item = substr($item, 4);
    }

    if ($lastOpeningPTag == '<p>') {
        $item = substr($item, 0, -3);
    }

    return $item;
}


function scp_init() {
	scp_add_extensions();
	scp_add_vc_custom_addons();

	require_once 'extensions/CPT.php';
	$layout_blocks = new CPT('layout_block');
}

function scp_activate() {
	scp_init();
	flush_rewrite_rules();
}

function scp_deactivate() {

}

function scp_add_extensions() {
	if ( apply_filters( 'scp_filter_enable_portfolio', false ) ) {
		require_once 'extensions/portfolio-post-type/portfolio-post-type.php';
	}
	if ( apply_filters( 'scp_filter_enable_post_subtitles', false ) ) {
		require_once 'extensions/easy-post-subtitle/easy-post-subtitle.php';
	}

	if ( ! scp_is_plugin_activating( 'smart-grid-gallery/smart-grid-gallery.php' ) && ! class_exists( 'SmartGridGallery' ) ) {
		require_once 'extensions/smart-grid-gallery/smart-grid-gallery.php';
	}
}

function scp_add_vc_custom_addons() {
	require_once 'vc-addons/embellishment/addon.php';
	require_once 'vc-addons/content-box/addon.php';
	require_once 'vc-addons/video-popup/addon.php';
	require_once 'vc-addons/logo/addon.php';
	require_once 'vc-addons/theme-button/addon.php';
	require_once 'vc-addons/theme-icon/addon.php';
	require_once 'vc-addons/theme-map/addon.php';
	require_once 'vc-addons/events/events.php';
	require_once 'vc-addons/post-list/addon.php';
	require_once 'vc-addons/pricing-plan/addon.php';
	require_once 'vc-addons/magnify-text/addon.php';
}

function scp_dynamic_section( $sections ) {

	$sections[] = array(
		'title'  => __( 'Widgets', SCP_TEXT_DOMAIN ),
		'desc'   => __( '<p class="description">This where you style widgets used mostly on the home page. Each subsection holds settings for custom widgets included with the theme.</p>', SCP_TEXT_DOMAIN ),
		'icon'   => 'el-icon-cog',
		// Leave this as a blank section, no options just some intro text set above.
		'fields' => array()
	);

	include 'vc-addons/our-process/redux-options.php';

	return $sections;

}


function scp_get_wheels_option( $option_name, $default = false ) {

	if ( function_exists( 'wheels_get_option' ) ) {
		return wheels_get_option( $option_name, $default );
	}

	return $default;
}

function scp_set_js_global_var() {

	$our_process_breakpoint = scp_get_wheels_option( 'dntp-our-process-widget-device-trigger', '480' );
	?>
	<script>
		var scp = scp ||
			{
				data: {
					vcWidgets: {
						ourProcess: {
							breakpoint: '<?php echo (int) $our_process_breakpoint; ?>'
						}
					}
				}
			};
	</script>
<?php
}

function scp_register_wp_widgets() {
	require_once 'wp-widgets/SCP_Latest_Posts_Widget.php';
}

function scp_portfolio_posts( $query ) {
	if ( is_admin() || ! $query->is_main_query() ) {
		return;
	}

	if ( is_tax() && isset( $query->tax_query ) && $query->tax_query->queries[0]['taxonomy'] == 'portfolio_category' ) {
		$query->set( 'posts_per_page', 10 );

		return;
	}
}

function scp_vc_editor_set_post_types() {
	$opt_name = 'scp_vc_post_types_set';
	$is_set   = (int) get_option( $opt_name );
	if ( ! $is_set ) {

		if ( function_exists( 'vc_editor_post_types' ) ) {

			$post_types = vc_editor_post_types();
			if ( ! in_array( 'layout_block', $post_types ) ) {
				$post_types[] = 'layout_block';
			}
			if ( ! in_array( 'course', $post_types ) ) {
				$post_types[] = 'course';
			}
			if ( ! in_array( 'teacher', $post_types ) ) {
				$post_types[] = 'teacher';
			}
			if ( ! in_array( 'events', $post_types ) ) {
				$post_types[] = 'events';
			}
			vc_editor_set_post_types( $post_types );
			add_option( $opt_name, true );
		}
	}
}

function scp_is_plugin_activating( $plugin ) {
	if ( isset( $_GET['action'] ) && $_GET['action'] == 'activate' && isset( $_GET['plugin'] ) ) {
		if ( $_GET['plugin'] == $plugin ) {
			return true;
		}
	}

	return false;
}

function scp_fpc( $filename, $filecontent ) {
	file_put_contents( $filename, $filecontent );
}

function scp_fgc( $filename ) {
	return file_get_contents( $filename );
}

function scp_get_theme_icon_list() {

	// class="(icon-\S+)\b" regex
	// array('$1' => '$1'),\n format

	$theme_icons = array(
		array('theme-icon-arrow-right' => 'theme-icon-arrow-right'),
		array('theme-icon-comment' => 'theme-icon-comment'),
		array('theme-icon-left-arrow' => 'theme-icon-left-arrow'),
		array('theme-icon-price-tag' => 'theme-icon-price-tag'),
		array('theme-icon-right-arrow' => 'theme-icon-right-arrow'),
		array('theme-icon-user' => 'theme-icon-user'),
		array('theme-icon-gardening' => 'theme-icon-gardening'),
		array('theme-icon-leaves' => 'theme-icon-leaves'),
		array('theme-icon-plant1' => 'theme-icon-plant1'),
		array('theme-icon-plant' => 'theme-icon-plant'),
		array('theme-icon-trees' => 'theme-icon-trees'),
		array('theme-icon-watering-can-with-water-drops' => 'theme-icon-watering-can-with-water-drops'),
		array('theme-icon-apple2' => 'theme-icon-apple2'),
		array('theme-icon-exercise1' => 'theme-icon-exercise1'),
		array('theme-icon-exercise' => 'theme-icon-exercise'),
		array('theme-icon-icon1' => 'theme-icon-icon1'),
		array('theme-icon-icon2' => 'theme-icon-icon2'),
		array('theme-icon-india' => 'theme-icon-india'),
		array('theme-icon-man' => 'theme-icon-man'),
		array('theme-icon-medical5' => 'theme-icon-medical5'),
		array('theme-icon-mountain' => 'theme-icon-mountain'),
		array('theme-icon-nature12' => 'theme-icon-nature12'),
		array('theme-icon-nature2' => 'theme-icon-nature2'),
		array('theme-icon-people4' => 'theme-icon-people4'),
		array('theme-icon-shape2' => 'theme-icon-shape2'),
		array('theme-icon-silhouette3' => 'theme-icon-silhouette3'),
		array('theme-icon-sports12' => 'theme-icon-sports12'),
		array('theme-icon-sports3' => 'theme-icon-sports3'),
		array('theme-icon-two3' => 'theme-icon-two3'),
		array('theme-icon-bath' => 'theme-icon-bath'),
		array('theme-icon-business3' => 'theme-icon-business3'),
		array('theme-icon-calculate' => 'theme-icon-calculate'),
		array('theme-icon-fashion' => 'theme-icon-fashion'),
		array('theme-icon-food13' => 'theme-icon-food13'),
		array('theme-icon-food11' => 'theme-icon-food11'),
		array('theme-icon-fruit9' => 'theme-icon-fruit9'),
		array('theme-icon-healthy2' => 'theme-icon-healthy2'),
		array('theme-icon-medical3' => 'theme-icon-medical3'),
		array('theme-icon-money2' => 'theme-icon-money2'),
		array('theme-icon-plus' => 'theme-icon-plus'),
		array('theme-icon-ribbon2' => 'theme-icon-ribbon2'),
		array('theme-icon-silhouette2' => 'theme-icon-silhouette2'),
		array('theme-icon-square' => 'theme-icon-square'),
		array('theme-icon-tool8' => 'theme-icon-tool8'),
		array('theme-icon-transport12' => 'theme-icon-transport12'),
		array('theme-icon-transport3' => 'theme-icon-transport3'),
		array('theme-icon-bag' => 'theme-icon-bag'),
		array('theme-icon-book12' => 'theme-icon-book12'),
		array('theme-icon-diploma' => 'theme-icon-diploma'),
		array('theme-icon-food10' => 'theme-icon-food10'),
		array('theme-icon-interface1' => 'theme-icon-interface1'),
		array('theme-icon-interface2' => 'theme-icon-interface2'),
		array('theme-icon-park1' => 'theme-icon-park1'),
		array('theme-icon-park' => 'theme-icon-park'),
		array('theme-icon-people1' => 'theme-icon-people1'),
		array('theme-icon-people22' => 'theme-icon-people22'),
		array('theme-icon-people3' => 'theme-icon-people3'),
		array('theme-icon-school' => 'theme-icon-school'),
		array('theme-icon-time4' => 'theme-icon-time4'),
		array('theme-icon-transport' => 'theme-icon-transport'),
		array('theme-icon-letter' => 'theme-icon-letter'),
		array('theme-icon-animal1' => 'theme-icon-animal1'),
		array('theme-icon-animal2' => 'theme-icon-animal2'),
		array('theme-icon-animal3' => 'theme-icon-animal3'),
		array('theme-icon-animal4' => 'theme-icon-animal4'),
		array('theme-icon-animal5' => 'theme-icon-animal5'),
		array('theme-icon-animal6' => 'theme-icon-animal6'),
		array('theme-icon-animal' => 'theme-icon-animal'),
		array('theme-icon-animals1' => 'theme-icon-animals1'),
		array('theme-icon-animals2' => 'theme-icon-animals2'),
		array('theme-icon-animals' => 'theme-icon-animals'),
		array('theme-icon-apple' => 'theme-icon-apple'),
		array('theme-icon-arrow' => 'theme-icon-arrow'),
		array('theme-icon-arrows' => 'theme-icon-arrows'),
		array('theme-icon-ball' => 'theme-icon-ball'),
		array('theme-icon-book1' => 'theme-icon-book1'),
		array('theme-icon-book' => 'theme-icon-book'),
		array('theme-icon-bookmark' => 'theme-icon-bookmark'),
		array('theme-icon-box' => 'theme-icon-box'),
		array('theme-icon-business' => 'theme-icon-business'),
		array('theme-icon-calendar' => 'theme-icon-calendar'),
		array('theme-icon-camera' => 'theme-icon-camera'),
		array('theme-icon-clock2' => 'theme-icon-clock2'),
		array('theme-icon-clock4' => 'theme-icon-clock4'),
		array('theme-icon-cup' => 'theme-icon-cup'),
		array('theme-icon-cut' => 'theme-icon-cut'),
		array('theme-icon-dog1' => 'theme-icon-dog1'),
		array('theme-icon-dog2' => 'theme-icon-dog2'),
		array('theme-icon-dog' => 'theme-icon-dog'),
		array('theme-icon-drink' => 'theme-icon-drink'),
		array('theme-icon-food1' => 'theme-icon-food1'),
		array('theme-icon-food2' => 'theme-icon-food2'),
		array('theme-icon-food3' => 'theme-icon-food3'),
		array('theme-icon-food4' => 'theme-icon-food4'),
		array('theme-icon-food' => 'theme-icon-food'),
		array('theme-icon-fruit1' => 'theme-icon-fruit1'),
		array('theme-icon-fruit2' => 'theme-icon-fruit2'),
		array('theme-icon-fruit3' => 'theme-icon-fruit3'),
		array('theme-icon-fruit4' => 'theme-icon-fruit4'),
		array('theme-icon-fruit5' => 'theme-icon-fruit5'),
		array('theme-icon-fruit6' => 'theme-icon-fruit6'),
		array('theme-icon-fruit7' => 'theme-icon-fruit7'),
		array('theme-icon-fruit' => 'theme-icon-fruit'),
		array('theme-icon-fruits' => 'theme-icon-fruits'),
		array('theme-icon-hand' => 'theme-icon-hand'),
		array('theme-icon-healthy1' => 'theme-icon-healthy1'),
		array('theme-icon-interface' => 'theme-icon-interface'),
		array('theme-icon-keys' => 'theme-icon-keys'),
		array('theme-icon-medical2' => 'theme-icon-medical2'),
		array('theme-icon-medical' => 'theme-icon-medical'),
		array('theme-icon-money' => 'theme-icon-money'),
		array('theme-icon-music1' => 'theme-icon-music1'),
		array('theme-icon-paint' => 'theme-icon-paint'),
		array('theme-icon-people' => 'theme-icon-people'),
		array('theme-icon-phone' => 'theme-icon-phone'),
		array('theme-icon-ribbon' => 'theme-icon-ribbon'),
		array('theme-icon-sewing' => 'theme-icon-sewing'),
		array('theme-icon-shape1' => 'theme-icon-shape1'),
		array('theme-icon-shape' => 'theme-icon-shape'),
		array('theme-icon-shield' => 'theme-icon-shield'),
		array('theme-icon-sign' => 'theme-icon-sign'),
		array('theme-icon-signs' => 'theme-icon-signs'),
		array('theme-icon-silhouette' => 'theme-icon-silhouette'),
		array('theme-icon-snack' => 'theme-icon-snack'),
		array('theme-icon-sport' => 'theme-icon-sport'),
		array('theme-icon-sports' => 'theme-icon-sports'),
		array('theme-icon-three' => 'theme-icon-three'),
		array('theme-icon-time1' => 'theme-icon-time1'),
		array('theme-icon-time2' => 'theme-icon-time2'),
		array('theme-icon-time3' => 'theme-icon-time3'),
		array('theme-icon-tool1' => 'theme-icon-tool1'),
		array('theme-icon-tool3' => 'theme-icon-tool3'),
		array('theme-icon-tool4' => 'theme-icon-tool4'),
		array('theme-icon-tool5' => 'theme-icon-tool5'),
		array('theme-icon-tool6' => 'theme-icon-tool6'),
		array('theme-icon-tool' => 'theme-icon-tool'),
		array('theme-icon-transport1' => 'theme-icon-transport1'),
		array('theme-icon-transport2' => 'theme-icon-transport2'),
		array('theme-icon-two' => 'theme-icon-two'),
		array('theme-icon-animal7' => 'theme-icon-animal7'),
		array('theme-icon-arrows2' => 'theme-icon-arrows2'),
		array('theme-icon-beach' => 'theme-icon-beach'),
		array('theme-icon-black' => 'theme-icon-black'),
		array('theme-icon-business2' => 'theme-icon-business2'),
		array('theme-icon-check' => 'theme-icon-check'),
		array('theme-icon-circle1' => 'theme-icon-circle1'),
		array('theme-icon-circle2' => 'theme-icon-circle2'),
		array('theme-icon-circle3' => 'theme-icon-circle3'),
		array('theme-icon-circle' => 'theme-icon-circle'),
		array('theme-icon-computer' => 'theme-icon-computer'),
		array('theme-icon-dog12' => 'theme-icon-dog12'),
		array('theme-icon-dog3' => 'theme-icon-dog3'),
		array('theme-icon-draw' => 'theme-icon-draw'),
		array('theme-icon-drink2' => 'theme-icon-drink2'),
		array('theme-icon-food12' => 'theme-icon-food12'),
		array('theme-icon-food22' => 'theme-icon-food22'),
		array('theme-icon-food32' => 'theme-icon-food32'),
		array('theme-icon-food42' => 'theme-icon-food42'),
		array('theme-icon-food5' => 'theme-icon-food5'),
		array('theme-icon-food6' => 'theme-icon-food6'),
		array('theme-icon-food7' => 'theme-icon-food7'),
		array('theme-icon-food8' => 'theme-icon-food8'),
		array('theme-icon-food9' => 'theme-icon-food9'),
		array('theme-icon-fruit12' => 'theme-icon-fruit12'),
		array('theme-icon-fruit8' => 'theme-icon-fruit8'),
		array('theme-icon-gps1' => 'theme-icon-gps1'),
		array('theme-icon-gps' => 'theme-icon-gps'),
		array('theme-icon-healthy' => 'theme-icon-healthy'),
		array('theme-icon-icon' => 'theme-icon-icon'),
		array('theme-icon-medical4' => 'theme-icon-medical4'),
		array('theme-icon-nature1' => 'theme-icon-nature1'),
		array('theme-icon-nature' => 'theme-icon-nature'),
		array('theme-icon-check-icon' => 'theme-icon-check-icon'),
		array('theme-icon-people2' => 'theme-icon-people2'),
		array('theme-icon-sewing2' => 'theme-icon-sewing2'),
		array('theme-icon-shapes1' => 'theme-icon-shapes1'),
		array('theme-icon-signs1' => 'theme-icon-signs1'),
		array('theme-icon-signs2' => 'theme-icon-signs2'),
		array('theme-icon-signs3' => 'theme-icon-signs3'),
		array('theme-icon-signs4' => 'theme-icon-signs4'),
		array('theme-icon-signs5' => 'theme-icon-signs5'),
		array('theme-icon-social' => 'theme-icon-social'),
		array('theme-icon-social-media1' => 'theme-icon-social-media1'),
		array('theme-icon-social-media2' => 'theme-icon-social-media2'),
		array('theme-icon-social-media3' => 'theme-icon-social-media3'),
		array('theme-icon-social-media' => 'theme-icon-social-media'),
		array('theme-icon-sports1' => 'theme-icon-sports1'),
		array('theme-icon-sports2' => 'theme-icon-sports2'),
		array('theme-icon-stack' => 'theme-icon-stack'),
		array('theme-icon-strength' => 'theme-icon-strength'),
		array('theme-icon-symbol' => 'theme-icon-symbol'),
		array('theme-icon-telephone' => 'theme-icon-telephone'),
		array('theme-icon-three2' => 'theme-icon-three2'),
		array('theme-icon-time' => 'theme-icon-time'),
		array('theme-icon-tool12' => 'theme-icon-tool12'),
		array('theme-icon-tool2' => 'theme-icon-tool2'),
		array('theme-icon-tool7' => 'theme-icon-tool7'),
		array('theme-icon-two2' => 'theme-icon-two2'),



	);

	return $theme_icons;
}
