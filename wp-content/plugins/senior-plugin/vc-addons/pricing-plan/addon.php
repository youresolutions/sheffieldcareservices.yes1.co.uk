<?php
/*
Plugin Name: Extend Visual Composer Plugin Example
Plugin URI: http://wpbakery.com/vc
Description: Extend Visual Composer with your own set of shortcodes.
Version: 0.1.1
Author: WPBakery
Author URI: http://wpbakery.com
License: GPLv2 or later
*/

/*
This example/starter plugin can be used to speed up Visual Composer plugins creation process.
More information can be found here: http://kb.wpbakery.com/index.php?title=Category:Visual_Composer
*/

// don't load directly
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

class SCP_Pricing_Plan {

	protected $shortcode_name = 'pricing_plan';
	protected $title = 'Pricing Plan';
	protected $description = '';
	protected $textdomain = 'care';

	function __construct() {
		// We safely integrate with VC with this hook
		add_action( 'init', array( $this, 'integrateWithVC' ) );

		// Use this when creating a shortcode addon
		add_shortcode( $this->shortcode_name, array( $this, 'render' ) );

		// Register CSS and JS
		add_action( 'wp_enqueue_scripts', array( $this, 'loadCssAndJs' ) );
	}

	public function integrateWithVC() {
		// Check if Visual Composer is installed
		if ( ! defined( 'WPB_VC_VERSION' ) ) {
			// Display notice that Visual Compser is required
			add_action( 'admin_notices', array( $this, 'showVcVersionNotice' ) );

			return;
		}

		/*
		Add your Visual Composer logic here.
		Lets call vc_map function to "register" our custom shortcode within Visual Composer interface.

		More info: http://kb.wpbakery.com/index.php?title=Vc_map
		*/
		vc_map( array(
			"name"        => __( $this->title, $this->textdomain ),
			"description" => __( $this->description, $this->textdomain ),
			"base"        => $this->shortcode_name,
			"class"       => "",
			"controls"    => "full",
			"icon"        => plugins_url( 'assets/aislin-vc-icon.png', __FILE__ ),
			// or css class name which you can reffer in your css file later. Example: "vc_extend_my_class"
			"category"    => __( 'Aislin', 'js_composer' ),
			//'admin_enqueue_js' => array(plugins_url('assets/vc_extend.js', __FILE__)), // This will load js file in the VC backend editor
			//'admin_enqueue_css' => array(plugins_url('assets/vc_extend_admin.css', __FILE__)), // This will load css file in the VC backend editor
			"params"      => array(
				array(
					'type'       => 'textfield',
					'holder'     => '',
					'class'      => '',
					'heading'    => __( 'Plan Title', $this->textdomain ),
					'param_name' => 'title',
					'value'      => __( '', $this->textdomain ),
				),
				array(
					'type'       => 'textfield',
					'holder'     => '',
					'class'      => '',
					'heading'    => __( 'Price', $this->textdomain ),
					'param_name' => 'price',
					'value'      => __( '', $this->textdomain ),
				),
				array(
					'type'       => 'textfield',
					'holder'     => '',
					'class'      => '',
					'heading'    => __( 'Currency Symbol', $this->textdomain ),
					'param_name' => 'currency_symbol',
					'value'      => __( '', $this->textdomain ),
				),
				array(
					'type'        => 'textarea',
					'holder'      => '',
					'class'       => '',
					'heading'     => __( 'Features', $this->textdomain ),
					'param_name'  => 'features',
					'value'       => __( '', $this->textdomain ),
					'description' => __( 'Pipe separated list (Feature 1 | Feature 2 | Feature 3).', $this->textdomain )
				),
				array(
					'type'       => 'textarea',
					'holder'     => '',
					'class'      => '',
					'heading'    => __( 'Footnote', $this->textdomain ),
					'param_name' => 'footnote',
					'value'      => __( '', $this->textdomain ),
				),
				array(
					'type'       => 'colorpicker',
					'heading'    => __( 'Background Color', $this->textdomain ),
					'param_name' => 'bg_color',
				),
				array(
					'type'       => 'colorpicker',
					'heading'    => __( 'Separator Color', $this->textdomain ),
					'param_name' => 'separator_color',
				),
				array(
					'type'       => 'colorpicker',
					'heading'    => __( 'Title/Price Color', $this->textdomain ),
					'param_name' => 'price_color',
				),
				array(
					'type'       => 'colorpicker',
					'heading'    => __( 'Feature Color', $this->textdomain ),
					'param_name' => 'feature_color',
				),
				array(
					'type'       => 'colorpicker',
					'heading'    => __( 'Icon Color', $this->textdomain ),
					'param_name' => 'icon_color',
				),
			)
		) );
	}

	/*
	Shortcode logic how it should be rendered
	*/
	public function render( $atts, $content = null ) {

		extract( shortcode_atts( array(
			'title'           => '',
			'price'           => '',
			'currency_symbol' => '',
			'features'        => '',
			'footnote'        => '',
			'bg_color'        => '',
			'separator_color' => '',
			'price_color'     => '',
			'feature_color'   => '',
			'icon_color'      => '',
		), $atts ) );


		$features = explode('|', $features);

		/**
		 * Price Box CSS
		 */
		$price_box_css = '';
		if ($price_color) {
			$price_box_css .= 'color:' . $price_color . ';';
		}

		if ($separator_color) {
			$price_box_css .= 'border-right:2px solid ' . $separator_color . ';';
		}

		if ($price_box_css) {
			$price_box_css = 'style="' . $price_box_css . '"';
		}

		/**
		 * BG CSS
		 */
		$bg_css = '';
		if ($bg_color) {
			$bg_css .= 'background-color:' . $bg_color;

			$bg_css = 'style="' . $bg_css . '"';
		}

		/**
		 * Feature Box CSS
		 */
		$feature_box_css = '';
		if ($feature_color) {
			$feature_box_css .= 'color:' . $feature_color;

			$feature_box_css = 'style="' . $feature_box_css . '"';
		}

		/**
		 * Icon CSS
		 */
		$icon_css = '';
		if ($icon_color) {
			$icon_css .= 'color:' . $icon_color;

			$icon_css = 'style="' . $icon_css . '"';
		}


		ob_start();
		?>
		<div class="wh-pricing-plan" <?php echo $bg_css; ?>>
			<div class="price-box" <?php echo $price_box_css; ?>>
				<div class="title"><?php echo esc_html($title); ?></div>
				<div class="price">
					<small><?php echo esc_html($currency_symbol); ?></small>
					<span><?php echo esc_html($price); ?></span>
				</div>
			</div>
			<div class="feature-box" <?php echo $feature_box_css; ?>>
				<ul>
					<?php foreach ($features as $feature) : ?>
						<li><i class="icon-check-icon" <?php echo $icon_css; ?>></i> <?php echo trim(esc_html($feature))?></li>
					<?php endforeach; ?>
				</ul>
				<div class="footnote">
					<?php echo esc_html($footnote); ?>
				</div>
			</div>
		</div>

		<?php
		$content = ob_get_clean();

		return $content;
	}

	/*
	Load plugin css and javascript files which you may need on front end of your site
	*/
	public function loadCssAndJs() {
		wp_register_style( 'vc_extend_style', plugins_url( 'assets/vc_extend.css', __FILE__ ) );
		wp_enqueue_style( 'vc_extend_style' );

		// If you need any javascript files on front end, here is how you can load them.
		//wp_enqueue_script( 'vc_extend_js', plugins_url('assets/vc_extend.js', __FILE__), array('jquery') );
	}

	/*
	Show notice if your plugin is activated but Visual Composer is not
	*/
	public function showVcVersionNotice() {
		$plugin_data = get_plugin_data( __FILE__ );
		echo '
        <div class="updated">
          <p>' . sprintf( __( '<strong>%s</strong> requires <strong><a href="http://bit.ly/vcomposer" target="_blank">Visual Composer</a></strong> plugin to be installed and activated on your site.', $this->textdomain ), $plugin_data['Name'] ) . '</p>
        </div>';
	}
}

// Finally initialize code
new SCP_Pricing_Plan();
