<?php // custom WordPress database error page

$message='There is a problem with the database! '.'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];

	header('HTTP/1.1 503 Service Temporarily Unavailable');
	header('Status: 503 Service Temporarily Unavailable');
	header('Retry-After: 3600'); // 1 hour = 3600 seconds
	mail("support@youresolutions.co.uk, connors@yesl.co.uk, rebeccac@yesl.co.uk, johnc@yesl.co.uk", "Database Error", $message, "From: thebrokendatabase");
	
?>
<!DOCTYPE HTML>
<html>
	<head>
		<title>503 Service Temporarily Unavailable</title>
		<style> type=textcss
			body{
			}
			h1, p {
				font-size: 24px;
				}
			p {
				font-size: 14px;
				}
			#body{
				text-align:center;
				font-family: arial;
				color:#006ea8;
			}
		</style>
	</head>
	<body>
		<div id="body">
		<img src="http://www.yesl.co.uk/images/logo.png" width="300px">
	  <h1>503 Service Temporarily Unavailable</h1>
		<h2>Captain, the ship can&rsquo;t take much more of this!</h2>
		<p>Sorry we are currently experiencing technical issues &mdash; Please check back soon!</p>
	</div>
	</body>
</html>