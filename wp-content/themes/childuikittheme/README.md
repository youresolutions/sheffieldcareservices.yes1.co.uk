# README #

This is a template for creating a child theme for the uikit theme

Installation
1. Replace the <Insert your site name here> with your site/company name in style.css
2. Drag and drop the childuikittheme in the wp-content/themes folder.
3. In WordPress select the Child Theme as the theme you want to use.

After installing you can freely change the css inside the child theme folder to your liking. Changes will only be reflected in the child theme.