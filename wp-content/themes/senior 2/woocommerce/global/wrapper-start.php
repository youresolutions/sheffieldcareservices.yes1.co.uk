<?php
/**
 * Content wrappers
 *
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

echo '<div class="' . wheels_class( 'container' ) . '"><div class="' . wheels_class( 'content' ) . '">';
