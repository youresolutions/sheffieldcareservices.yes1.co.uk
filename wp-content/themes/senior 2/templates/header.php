<?php
global $post_id;
$use_top_bar            = wheels_get_option( 'top-bar-use', false );
$use_top_bar_additional = wheels_get_option( 'top-bar-additional-use', false );

$top_bar_layout_block = wheels_get_layout_block( 'top-bar-layout-block' );
$logo_location        = wheels_get_option( 'logo-location', 'main_menu' );
$use_logo             = $logo_location == 'main_menu' ? true : false;
?>

<?php if ( $top_bar_layout_block ): ?>
	<div class="<?php echo wheels_class( 'container_top_bar_layout_block' ); ?>">
		<?php echo do_shortcode( $top_bar_layout_block->post_content ); ?>
	</div>
<?php endif; ?>

<header class="<?php echo wheels_class( 'header' ); ?>">
	<?php if ( $use_top_bar ): ?>
		<?php get_template_part( 'templates/top-bar' ); ?>
	<?php endif; ?>
	<?php if ( $use_top_bar_additional ): ?>
		<?php get_template_part( 'templates/top-bar-additional' ); ?>
	<?php endif; ?>
	<div class="<?php echo wheels_class( 'main-menu-bar-wrapper' ); ?>">
		<div class="<?php echo wheels_class( 'container' ); ?>">
			<?php if ( $use_logo ): ?>
				<div class="<?php echo wheels_class( 'logo-wrapper' ); ?>">
					<?php get_template_part( 'templates/logo' ); ?>
				</div>
			<?php endif; ?>
			<div class="<?php echo wheels_class( 'main-menu-wrapper' ); ?>">
				<?php get_template_part( 'templates/menu-main' ); ?>
			</div>
		</div>
	</div>
</header>
