<?php
/**
 * Theme activation
 */
if ( is_admin() && isset( $_GET['activated'] ) && 'themes.php' == $GLOBALS['pagenow'] ) {
	wp_redirect( admin_url( 'themes.php?page=theme_activation_options' ) );
	exit;
}
add_action( 'admin_menu', 'wheels_theme_activation_options_add_page', 50 );
add_action( 'admin_init', 'wheels_import_data' );


function wheels_theme_activation_options_add_page() {

	add_theme_page( __( 'Theme Activation', 'wheels' ), __( 'Theme Activation', 'wheels' ), 'edit_theme_options', 'theme_activation_options', 'wheels_theme_activation_options_render_page' );
}

function wheels_import_data() {

	if ( isset( $_POST['wheels-demo-data'] ) ) {
		if ( check_admin_referer( 'wheels-demo-data-nonce' ) ) {

			require_once 'demo-importer/Wheels_Import_Manager.php';
			$import_manager = new Wheels_Import_Manager();

			/**
			 * Import Theme Options
			 */
			if ( isset( $_REQUEST['theme_options'] ) && $_REQUEST['theme_options'] != '' ) {
				$theme_options_filename = 'theme-options/' . $_REQUEST['theme_options'] . '.json';
				$import_manager->import_theme_options( $theme_options_filename );
			}

		}
	}

}

function wheels_theme_activation_options_render_page() {
	?>
	<div class="wrap">
		<h2><?php printf( __( '%s Theme Activation', 'wheels' ), wp_get_theme() ); ?></h2>

		<div class="update-nag">
			<?php _e( 'These settings are optional and should usually be used only on a fresh installation.', 'wheels' ); ?>
		</div>
		<p>
			<a style="margin-right: 20px;" target="_blank" href="https://www.youtube.com/watch?v=bRnThJE0HCc"><img src="<?php echo get_template_directory_uri() . '/assets/img/theme-installation-video-thumb.png'?>" alt=""/></a>
			<a target="_blank" href="https://www.youtube.com/watch?v=aHR7iWr-iWE"><img src="<?php echo get_template_directory_uri() . '/assets/img/theme-update-video-thumb.png'?>" alt=""/></a>
		</p>
		<br/>
		<hr/>
		<h3><?php _e( 'Update Instructions', 'wheels' ); ?></h3>
		<p>
		<?php _e( 'If you are updating the theme, please watch the video on the right.',
						'wheels' ); ?>
		</p>
		<p>
		<?php _e( 'If you are updating to version 2x, follow the update process',
						'wheels' ); ?>
		<a href="<?php echo admin_url( 'admin.php?page=theme_update' ); ?>">here</a>.
		</p>
		<hr/>
		<h3><?php _e( 'Installation Steps', 'wheels' ); ?></h3>

		<ol>
			<li>
				<p><strong><?php _e( 'Install required plugins', 'wheels' ); ?></strong></p>
				<p><?php _e( 'First, enable required plugins. If you have not already done so, there should be a link "Install Required Plugins" in the top of the page. Follow that link and after you finish plugin installation return to this page to complete the installation process.',
						'wheels' ); ?></p>
				<h5>(Ultimate Addons for Visual Composer is required for Default demo. You don't need to install if you are using Nurse or Agency demo)</h5>
			</li>
			<li>
				<p><strong><?php _e( 'Import demo content', 'wheels' ); ?></strong></p>
				<p><?php _e( 'Proceed only after all plugins are installed', 'wheels' ); ?></p>
				<?php if ( is_plugin_active( 'wordpress-importer/wordpress-importer.php' ) ): ?>
					<a href="<?php echo admin_url( 'import.php?import=wordpress' ); ?>"><?php _e('Go to Wordpress Importer', 'wheels'); ?></a>
				<?php else: ?>
					<?php _e( 'In order to import demo content you need to install and activate Wordpress Importer plugin', 'wheels' ); ?>
				<?php endif; ?>
			</li>
			<li>
				<p><strong><?php _e( 'Import Layer Slider demo sliders', 'wheels' ); ?></strong></p>
				<?php if ( is_plugin_active( 'LayerSlider/layerslider.php' ) ): ?>
					<a href="<?php echo admin_url( 'admin.php?page=layerslider' ); ?>"><?php _e('Go to Layer Slider Importer', 'wheels'); ?></a>
				<?php else: ?>
					<?php _e( 'In order to import demo sliders you need to install and activate Layer Slider plugin', 'wheels' ); ?>
				<?php endif; ?>
			</li>
			<li>
				<h5>(Required for Default demo. You can skip this step if you are using Nurse or Agency demo)</h5>
				<p><strong><?php _e( 'Import Icons', 'wheels' ); ?></strong></p>
				<a href="<?php echo admin_url( 'admin.php?page=bsf-font-icon-manager' ); ?>"><?php _e('Go to Ultimate VC Addons Icon Manager', 'wheels'); ?></a>
			</li>
			<li>
				<p><strong><?php _e( 'Save Menus', 'wheels' ); ?></strong></p>
				<a href="<?php echo admin_url( 'nav-menus.php' ); ?>"><?php _e('Go to Wordpress Menus', 'wheels'); ?></a>
			</li>
			<li>
				<p><strong><?php _e( 'Select Front and Posts page', 'skilled' ); ?></strong></p>
				<a href="<?php echo admin_url( 'options-reading.php' ); ?>"><?php esc_html_e('Go to Wordpress Reading Settings', 'skilled'); ?></a>
			</li>
		</ol>

		<br/>
		<hr/>
		<br/>

		<form method="post" action="<?php echo esc_url( $_SERVER['REQUEST_URI'] ); ?>">
			<input type="hidden" name="wheels-demo-data" value="1">
			<?php wp_nonce_field( 'wheels-demo-data-nonce' ); ?>
			<table class="form-table">


				<tr valign="top">
					<th scope="row"><?php esc_html_e( 'Import Theme Options', 'care' ); ?></th>
					<td>
						<fieldset>
							<select name="theme_options" id="">
								<option value="">Select Color Variation</option>
								<option value="default">Senior Care - Default</option>
								<option value="grey-turquise">Senior Care - Turquise</option>
								<option value="peach-blue">Senior Care - Peach Blue</option>
								<option value="purple">Senior Care - Purple</option>
								<option value="red-grey">Senior Care - Red Grey</option>
								<option value="senior-agency">Senior Agency</option>
								<option value="senior-nurse">Senior Nurse</option>
							</select>
						</fieldset>
					</td>
				</tr>
			</table>
			<?php submit_button(); ?>
		</form>
		<br/>
		<hr/>
		<br/>
		<h3>
			<em><?php printf( __( 'Visit our FAQ page %s.', 'wheels' ),
					'<a target="_blank" href="http://aislinthemes.com/faq/" title="' . __( 'faq', 'wheels' ) . '">' . __( 'here', 'wheels' ) . '</a>' ); ?></em>
		</h3>
		<br/>
		<hr/>
		<br/>
	</div>

<?php

}
