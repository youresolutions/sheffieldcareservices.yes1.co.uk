<?php
/**
 * Default template for sidebar
 */
?>

<?php if (is_active_sidebar('sidebar-secondaryproduct')) : ?>
    <div id="product-sidebar-right" class="widget-area" role="complementary" style="padding:10px;">        
        <?php dynamic_sidebar('sidebar-secondaryproduct'); ?>
    </div>
<?php endif; ?>
