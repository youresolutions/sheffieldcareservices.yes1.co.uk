<?php
/**
 * The Header template
 *
 * Displays all of the <head> section and everything up till
 * and the opening body-tag
 */


?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?> class="uk-height-1-1">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?> class="uk-height-1-1">
<![endif]-->
<!--[if IE 9]>
<html class="ie ie9" <?php language_attributes(); ?> class="uk-height-1-1">
<![endif]-->
<!--[if !(IE 7) | !(IE 8) | !(IE 9)  ]><!-->
<html <?php language_attributes(); ?> >
<!--<![endif]-->
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title('-', true, 'right'); ?></title>

    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>

    <?php wp_head(); ?>
</head>

<?php if (mytheme_option( 'fullscreen' )) { $width = 'uk-custom-bg'; } else { $width = 'uk-custom-width'; } ?>

<!--<body <?php body_class(array($width)); ?>>-->
<body <?php body_class(array()); ?>>
<div class="se-pre-con"></div>   
<div id="wrapper" class="stickyfooter <?php if (mytheme_option( 'fullscreen' )) { echo 'uk-custom-bg'; } else { echo 'uk-custom-width'; } ?>">
