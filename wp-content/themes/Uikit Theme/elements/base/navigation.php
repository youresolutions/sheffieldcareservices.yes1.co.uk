<?php
/**
 * element for navigation and offcanvas navigation
 */

$nav = wp_nav_menu(array(
    'theme_location' => 'main',
    'menu_class'     => 'uk-navbar-nav nodisplay uk-nav-parent-icon',
    'depth'          => 2,
    'walker'         => new WordpressUikitMenuWalker('navbar'),
    'echo'           => false,
    'fallback_cb'    => false
));

$nav_offcanvas = wp_nav_menu(array(
    'theme_location' => 'main',
    'menu_class'     => 'uk-nav uk-nav-offcanvas uk-nav-parent-icon',
    'depth'          => 2,
    'walker'         => new WordpressUikitMenuWalker('offcanvas'),
    'echo'           => false,
    'fallback_cb'    => false,
    'items_wrap' => '<ul class="%2$s" data-uk-nav="">%3$s</ul>'
));

if (mytheme_option('secondtopnav')) {

$nav_secondary = wp_nav_menu(array(
    'theme_location' => 'secondary',
    'menu_class'     => 'uk-navbar-nav nodisplay uk-nav-parent-icon',
    'depth'          => 2,
    'walker'         => new WordpressUikitMenuWalker('navbar'),
    'echo'           => false,
    'fallback_cb'    => false
));

$nav_secondaryoffcanvas = wp_nav_menu(array(
    'theme_location' => 'secondary',
    'menu_class'     => 'uk-nav uk-nav-offcanvas uk-nav-parent-icon',
    'depth'          => 2,
    'walker'         => new WordpressUikitMenuWalker('offcanvas'),
    'echo'           => false,
    'fallback_cb'    => false,
    'items_wrap' => '<ul class="%2$s" data-uk-nav="">%3$s</ul>'
));
}
?> <div id="nav"> <?php
if (mytheme_option('stickysocial') && mytheme_option('socialmediabar')) { $stickyclass = 'sticky-header-withsocial'; } else { $stickyclass = 'sticky-header'; }
if (mytheme_option('socialmediamob') && mytheme_option('socialmediabar')) { $stickyclass='sticky-header-withsocialmob'; }
?>
<?php
    if ($nav) :
    if ((mytheme_option( 'fullscreen' )) && (mytheme_option( 'header' ))) {?>
            <div id="" style="" <?php if (mytheme_option('stickynav')) { echo 'data-uk-sticky="{clsactive:\'uk-active uk-sticky-nav '.$stickyclass.'\'}"'; } ?>>
            <?php  }
        elseif((!mytheme_option( 'header' )) && (mytheme_option( 'fullscreen' )) ) { ?>
            <div style="" <?php if (mytheme_option('stickynav')) { echo 'data-uk-sticky="{clsactive:\'uk-active uk-sticky-nav '.$stickyclass.'\'}"'; } ?>>
            <?php }
        elseif((mytheme_option( 'header' )) && (!mytheme_option( 'fullscreen' )) ) { ?>
            <div class="" <?php if (mytheme_option('stickynav')) { echo 'data-uk-sticky="{clsactive:\'uk-active uk-sticky-nav '.$stickyclass.'\'}"'; } ?>>
        <?php } else { ?>
            <div class="" <?php if (mytheme_option('stickynav')) { echo 'data-uk-sticky="{clsactive:\'uk-active uk-sticky-nav '.$stickyclass.'\'}"'; } ?>>
        <?php } ?>
   <nav class="uk-navbar">
    <div class="<?php if (mytheme_option('contentwidth')) { echo 'uk-content-width'; } ?>">
    <a href="#offcanvas-menu" class="uk-navbar-toggle mobile" data-uk-offcanvas></a>
    <?php if ((!mytheme_option( 'headermob' )) || (!mytheme_option( 'header' ))) { if (trim(mytheme_option( 'mobilelogo' )) != '') { ?> <a href="<?php echo get_bloginfo('url'); ?>" class="uk-navbar-brand mobile uk-navbar-center"><img src="<?php echo mytheme_option( 'mobilelogo' ) ?>"></a> <?php }
    else { if (trim(mytheme_option( 'headerlogo' )) != '') { ?> <a href="<?php echo get_bloginfo('url'); ?>" class="uk-navbar-brand mobile uk-navbar-center"><img src="<?php echo mytheme_option( 'headerlogo' ) ?>" style="width:<?php if (trim(mytheme_option( 'logowidth' )) != '') { echo mytheme_option( 'logowidth' ) . ""; } ?>; height:<?php if (trim(mytheme_option( 'logoheight' )) != '') { echo mytheme_option( 'logoheight' ) . ""; } ?>"></a><?php } } } ?>
    <?php if ((mytheme_option( 'stickynav' )) && (mytheme_option( 'stickynavlogo' )) && (mytheme_option( 'header' )))  { ?> <a href="<?php echo get_bloginfo('url'); ?>" class="uk-navbar-brand uk-sticky-logo"><img src="<?php if (trim(mytheme_option( 'stickynavlogoimg' )) != '') { echo mytheme_option( 'stickynavlogoimg' ); } else { echo mytheme_option( 'headerlogo' ); } ?>" style="max-height:40px"></a> <?php } ?>
    <?php if (!mytheme_option( 'header' )) { ?>
    <?php if (trim(mytheme_option( 'headerlogo' )) != '') { ?> <a href="<?php echo get_bloginfo('url'); ?>" class="uk-navbar-brand nodisplay"><img src="<?php echo mytheme_option( 'headerlogo' ) ?>" style="width:<?php if (trim(mytheme_option( 'logowidth' )) != '') { echo mytheme_option( 'logowidth' ) . ""; } ?>; height:<?php if (trim(mytheme_option( 'logoheight' )) != '') { echo mytheme_option( 'logoheight' ) . ""; } ?>"></a>
    <ul class="uk-navbar-nav nodisplay"> <?php } else { ?>
    <ul class="uk-navbar-nav nodisplay"> <?php } ?>
    <?php } else { ?>
    <ul class="uk-navbar-nav nodisplay">
    <?php } ?>
            <?php echo $nav ?>
     </ul>
    <?php if (trim(mytheme_option('navbarextramob')) !='' ) { echo '<ul class="uk-navbar-nav uk-navbar-center mobile">'.(mytheme_option('navbarextramob')).'</ul>'; } ?>
     <div class="uk-navbar-flip">
    <?php if (class_exists('woocommerce')){ ?>
    <ul class="uk-navbar-nav nodisplay"><li id="basket" class="uk-navbar-nav" style="margin-right:5px;" data-uk-dropdown="{pos:'bottom-right'}"><?php echo uikit_get_cart_button_html() ?>
     <div class="uk-dropdown uk-dropdown-navbar"><?php the_widget( 'WC_Widget_Cart_Navbar','title=Cart' ); ?></div></li></ul> <?php } ?>
       <?php if (trim(mytheme_option('navbarextra')) !='' ) { echo '<ul class="uk-navbar-nav nodisplay">'.(mytheme_option('navbarextra')).'</ul>'; } ?>

         <?php if (mytheme_option('navsearch')) { ?>
            <div class="uk-navbar-content nodisplay">
                    <form class="uk-search" method="get" id="search_form" action="<?php bloginfo('home'); ?>"/>
                       <input type="text" class="uk-search-field" name="s" value="" Placeholder="Search" >
                    </form>
            </div>

         <?php } ?>
    </div>
    </div>
    </nav>
    <div id="offcanvas-menu" class="uk-offcanvas">
        <div class="uk-offcanvas-bar">
            <?php echo $nav_offcanvas ?>
            <?php if (mytheme_option('secondtopnav')) { echo $nav_secondaryoffcanvas; } ?>
            <?php if (trim(mytheme_option('navbarextra')) !='' ) { echo '<ul class="uk-navbar-nav mobile">'.(mytheme_option('navbarextra')).'</ul>'; } ?>
            <?php if (mytheme_option('navsearch')) { ?>
            <div class="uk-navbar-content mobile">
                    <form class="uk-search" method="get" id="search_form" action="<?php bloginfo('home'); ?>"/>
                       <input type="text" class="uk-search-field" name="s" value="" Placeholder="Search" >
                    </form>
            </div>
            <?php } ?>

    </div>
</div>
</div>
<?php endif; ?>


<div class="uk-clearfix"></div>

<?php
if (mytheme_option('secondtopnav')) { ?>

<nav class="uk-navbar-secondary">
    <div class="<?php if (mytheme_option('contentwidth')) { echo 'uk-content-width'; } ?>">
     <ul class="uk-navbar-nav uk-navbar-nav-secondary nodisplay">
            <?php echo $nav_secondary ?>
     </ul>
   </div>
</nav>

<div class="uk-clearfix"></div>
<?php } ?>
</div>

<?php if (is_front_page() || mytheme_option('sliderallpages')) {
    if (mytheme_option('slider')) {
        get_template_part('elements/content/ThemeSlideshow');
} }
?>
