<?php
/**
 * element for footer
 */

$nav_footer = wp_nav_menu(array(
    'theme_location' => 'footer',
    'menu_class'     => 'uk-subnav uk-subnav-line',
    'depth'          => 1,
    'walker'         => new WordpressUikitMenuWalker('inline'),
    'echo'           => false,
    'fallback_cb'    => false
));

?>
<footer id="footer" class="footer">
  <?php if (mytheme_option('scrolltotop')) { ?>
    <button onclick="topFunction()" id="TopScroll" title="Go to top"><?php if (trim(mytheme_option( 'scrolltext' )) != '') { echo mytheme_option('scrolltext'); } else { echo 'Top'; } ?></button> <?php } ?>
    <div id="footersidebar">
        <?php if (!mytheme_option('fullscreen')) { ?>
        <div class="uk-container uk-container-center <?php if (mytheme_option('contentwidth')) { echo 'uk-content-width'; } ?>">
        <?php } else { ?>
        <div class="uk-container uk-container-center <?php if (mytheme_option('contentwidth')) { echo 'uk-content-width'; } ?>">
        <?php } ?>
        <?php get_sidebar('footer'); ?>
        </div>
    </div>
    <div id="footerbg" style="background-color:<?php echo mytheme_option("footerbg"); ?>;">
    <?php if (!mytheme_option('fullscreen')) { ?>
    <div class="uk-container uk-container-center <?php if (mytheme_option('contentwidth')) { echo 'uk-content-width'; } ?>">
        <?php } else { ?>
        <div class="uk-container uk-container-center <?php if (mytheme_option('contentwidth')) { echo 'uk-content-width'; } ?>">
        <?php } ?>
        <?php if ($nav_footer) : ?>
            <nav class="uk-text-center">
                <?php echo $nav_footer ?>
            </nav>
        <?php endif; ?>

        <div class="uk-text-center">
            <div class="uk-panel" style="color:<?php echo (mytheme_option('footertextcol')); ?>">
                <?php echo mytheme_option('footertext'); ?>

            </div>
        </div>
    </div>
    </div>
</footer>
