<?php
/**
 * element pre content
 * should be rendered directly before the dynamic content
 * handles displaying of the sidebar (or no sidebar)
 * must be closed via the element postcontent
 */
 if (mytheme_option('twosidebars')) {
?>
<?php if (mytheme_option('fullscreen')) { ?>
<section id="main" class="<?php if (mytheme_option('fixedfooter')) { echo 'footer-padding'; } ?>">
    <div id="contentwrapper" class="<?php if ((mytheme_option('contentwidth')) && (mytheme_option('pagepadding'))) { echo 'uk-content-width'; } ?>">
<?php } else { ?>
  <section id="main" class="<?php if (mytheme_option('fixedfooter')) { echo 'footer-padding'; } ?>">
      <div id="contentwrapper" class="<?php if (mytheme_option('contentwidth')) { echo 'uk-content-width'; } ?>">
<?php } ?>
<?php if (mytheme_option('breadcrumbs')) { ?><div class="breadcrumbs"><?php the_breadcrumb(); ?></div><?php } ?>
        <div class="uk-grid" style="margin-left:0px; margin-right:0px;">
             <?php if ((is_front_page() == false) || ((is_front_page()) && (mytheme_option( 'frontsidebar' )))): ?>
                    <div class="uk-width-twosidebar uk-sidebar-bg uk-sidebar-border-right <?php if (!mytheme_option( 'mobilesidebar' )) { echo 'nodisplay'; } ?> <?php if (mytheme_option( 'sidebarlast' )) { echo 'showlastleftsidebar'; } ?> <?php if (mytheme_option( 'centersidebarmob' )) { echo 'centersidebar'; } ?>"  style="padding-left:0px; padding-right:0px;">
                           <?php get_sidebar('sidebar-main'); ?>
                    </div>
                    <div id="maincontent" class="uk-width-twosidebarcontent" style="padding-left:0px">
                        <?php else : ?>
                    <div class="uk-width-1-1" style="padding-right:0px;">
                <?php endif; ?>
            <section id="content">

<?php } else { ?>

<?php if ((mytheme_option('fullscreen')) || !(is_active_sidebar( 'sidebar-main' ))) { ?>
<section id="main" class="<?php if (mytheme_option('fixedfooter')) { echo 'footer-padding'; } ?>">
    <div id="contentwrapper" class="<?php if ((mytheme_option('contentwidth')) && (mytheme_option('pagepadding'))) { echo 'uk-content-width'; } ?>">
<?php } else { ?>
  <section id="main" class="<?php if (mytheme_option('fixedfooter')) { echo 'footer-padding'; } ?>">
      <div id="contentwrapper" class="<?php if (mytheme_option('contentwidth')) { echo 'uk-content-width'; } ?>">
<?php } ?>
<?php if (mytheme_option('breadcrumbs')) { ?><div class="breadcrumbs"><?php the_breadcrumb(); ?></div><?php } ?>
        <div class="uk-grid" style="margin-left:0px; margin-right:0px;">
            <?php if (mytheme_option('sidebarloc') == 'left') { ?>
            <?php if ( is_active_sidebar( 'sidebar-main' ) ) : ?>
             <?php if ((is_front_page() == false) || ((is_front_page()) && (mytheme_option( 'frontsidebar' )))): ?>
                    <div class="uk-width-onesidebar uk-sidebar-bg uk-sidebar-border-right <?php if (!mytheme_option( 'mobilesidebar' )) { echo 'nodisplay'; } ?> <?php if (mytheme_option( 'sidebarlast' )) { echo 'showlastleftsidebar'; } ?> <?php if (mytheme_option( 'centersidebarmob' )) { echo 'centersidebar'; } ?>" style="padding-right:0px; padding-left:0px;">
                           <?php get_sidebar(); ?>
                    </div>
            <?php endif; ?>
            <?php endif; ?>
            <?php } ?>
            <?php wp_reset_query(); ?>
            <?php if ( is_active_sidebar( 'sidebar-main' ) ) : ?>
                <?php if ((is_front_page() == false) || ((is_front_page()) && (mytheme_option( 'frontsidebar' )))): ?>
                    <div id="maincontent" class="uk-width-onesidebarcontent" style="padding-left:0px">
                        <?php else : ?>
                    <div id="maincontent" class="uk-width-1-1" style="padding-left:0px;">
                <?php endif; ?>
                <?php else : ?>
                    <div id="maincontent" class="uk-width-1-1" style="padding-left:0px;">
            <?php endif; ?>
            <section id="content">

<?php } ?>
