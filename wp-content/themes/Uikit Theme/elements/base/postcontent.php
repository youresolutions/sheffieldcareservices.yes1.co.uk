<?php
/**
 * element post content
 * should be rendered directly after the dynamic content
 * handles displaying of the sidebar (or no sidebar)
 * must be opend via the element precontent
 */
  if (mytheme_option('twosidebars')) {
?>

                </section>
            </div>


             <?php if ((is_front_page() == false) || ((is_front_page()) && (mytheme_option( 'frontsidebar' )))): ?>
                    <div class="uk-width-twosidebar uk-sidebar-bg uk-sidebar-border-left <?php if (!mytheme_option( 'mobilesidebar' )) { echo 'nodisplay'; } ?> <?php if (mytheme_option( 'sidebarlast' )) { echo 'showlastrightsidebar'; } ?> <?php if (mytheme_option( 'centersidebarmob' )) { echo 'centersidebar'; } ?>" style="padding-left:0px; padding-right:0px;">
                           <?php get_sidebar( 'secondary' ); ?>
                    </div>
            <?php endif; ?>


        </div>
  </div>
</section>

<?php } else { ?>

                </section>
            </div>
            <?php if (mytheme_option('sidebarloc') == 'right') { ?>
            <?php if ( is_active_sidebar( 'sidebar-main' ) ) : ?>
             <?php if ((is_front_page() == false) || ((is_front_page()) && (mytheme_option( 'frontsidebar' )))): ?>
                    <div class="uk-width-onesidebar uk-sidebar-bg uk-sidebar-border-left <?php if (!mytheme_option( 'mobilesidebar' )) { echo 'nodisplay'; } ?> <?php if (mytheme_option( 'sidebarlast' )) { echo 'showlastrightsidebar'; } ?> <?php if (mytheme_option( 'centersidebarmob' )) { echo 'centersidebar'; } ?>"  style="padding-left:0px; padding-right:0px;">
                           <?php get_sidebar(); ?>
                    </div>
            <?php endif; ?>
            <?php endif; ?>
            <?php } ?>
        </div>
    </div>
</section>


<?php } ?>
