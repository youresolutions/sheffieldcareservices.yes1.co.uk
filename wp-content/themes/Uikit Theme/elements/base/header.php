<?php
/**
 * element for header
 */
 if (mytheme_option('socialmediabar')) { ?>
<div id="socialmedia" class="<?php if (mytheme_option('socialmediamob')) { echo 'mobile'; } ?>">
   <div class="uk-fixed-socialbar" style="background-color:<?php echo mytheme_option('smbarcol'); ?>" <?php if (mytheme_option('stickysocial')) { echo 'data-uk-sticky="{clsactive:\'uk-active sticky-header\'}"'; } ?>>
       <div class="<?php if (mytheme_option('contentwidth')) { echo 'uk-content-width'; } ?>">
           <div class="uk-flex uk-flex-social" style="padding-top:5px;padding-bottom:5px;">
               <?php if (trim(mytheme_option('socialmediafb')) != '') { ?><a href="<?php echo mytheme_option('socialmediafb'); ?>" target="_blank" class="uk-icon-button-small uk-icon-facebook"></a> <?php } ?>
               <?php if (trim(mytheme_option('socialmediatwit')) != '') { ?><a href="<?php echo mytheme_option('socialmediatwit'); ?>" target="_blank" class="uk-icon-button-small uk-icon-twitter"></a> <?php } ?>
               <?php if (trim(mytheme_option('socialmediainst')) != '') { ?><a href="<?php echo mytheme_option('socialmediainst'); ?>" target="_blank" class="uk-icon-button-small uk-icon-instagram"></a> <?php } ?>
               <?php if (trim(mytheme_option('socialmediali')) != '') { ?><a href="<?php echo mytheme_option('socialmediali'); ?>" target="_blank" class="uk-icon-button-small uk-icon-linkedin"></a> <?php } ?>
               <?php if (trim(mytheme_option('socialmediaphone')) != '') { ?><a href="tel:<?php echo mytheme_option('socialmediaphone'); ?>" target="_top" class="uk-icon-button-small uk-icon-phone"></a> <?php } ?>
               <?php if (trim(mytheme_option('socialmediaemail')) != '') { ?><a href="mailto:<?php echo mytheme_option('socialmediaemail'); ?>?Subject=General%20Enquiry" target="_top" class="uk-icon-button-small uk-icon-envelope"></a> <?php } ?>
               <div class="uk-contrast"><?php echo mytheme_option('socialmediabuttons'); ?></div>

                   <?php if (class_exists('woocommerce')){ ?>
                   <div class="mobile"><div id="basket" class="" style="" data-uk-dropdown="{justify:'#socialmedia'}"><?php echo uikit_get_cart_button_html() ?>
                    <div class="uk-dropdown"><?php the_widget( 'WC_Widget_Cart_Navbar','title=Cart' ); ?></div></div></div> <?php } ?>

           </div>
       </div>
   </div>
</div>


<?php }
if (mytheme_option( 'header' )) {
     $header_style = '';
    /*if ($headerImageUrl = get_header_image()) {
        $header_style = 'style="height: 180px; background-image: url(' . $headerImageUrl . ');"';
    }*/
if (get_header_image()=='') {
     $header_style = 'style="min-height: ' . mytheme_option('headerheight') . '; background-color: ' . mytheme_option( 'headercolour' ) . ';"';
    }
    else {
        $header_style = 'style="min-height: ' . mytheme_option('headerheight') . '; background-image: url(' . get_header_image() . ');"';

    }
    $text_style = '';
 if (get_header_textcolor()=='blank') {
     $text_style='style="display:none;"'; }
    else {
        $text_style='style="color:' . mytheme_option( 'headertextcolour' ) . ';"';

    }

?>

<header id="header" class="uk-cover-background uk-contrast <?php if (!mytheme_option( 'headermob' )) { echo 'nodisplay'; } ?>" style="background-color:<?php echo mytheme_option( 'headercolour' );?>" >
  <div class="<?php if (mytheme_option('contentwidth')) { echo 'uk-content-width'; } ?>">

    <?php if (mytheme_option('headerslider'))  { ?>


          <div class="<?php if (mytheme_option('hslidermobdis')) { echo 'nodisplay';} ?> uk-slidenav-position <?php echo mytheme_option('hoverlaydisp'); ?>" data-uk-slideshow="{ <?php if (mytheme_option('hkenburns')) { echo 'kenburns:true,'; } ?> <?php if (mytheme_option('hautoplay')) { echo 'autoplay:true,'; } ?> <?php echo mytheme_option('hanim'); ?> }">
            <ul class="uk-slideshow">

                <?php if (trim(mytheme_option( 'hslideone' )) != '') { ?>
                <li>

                      <img src="<?php echo mytheme_option( 'hslideone' ); ?>">




                </li>
             <?php }
                if (trim(mytheme_option( 'hslidetwo' )) != '') { ?>
                <li>

                      <img src="<?php echo mytheme_option( 'hslidetwo' ); ?>">


        </li>
        <?php }
        if (trim(mytheme_option( 'hslidethr' )) != '') { ?>
        <li>

              <img src="<?php echo mytheme_option( 'hslidethr' ); ?>">


        </li>
        <?php }
        if (trim(mytheme_option( 'hslidefr' )) != '') { ?>
        <li>

              <img src="<?php echo mytheme_option( 'hslidefr' ); ?>">


        </li>
        <?php } ?>

      </ul>

      <figcaption class="uk-overlay-panel <?php if (mytheme_option( 'hremovepadding' )) { echo 'uk-padding-remove'; }; ?> <?php echo mytheme_option( 'holbg' ); ?> <?php echo mytheme_option( 'hoverlayanim' ); ?> uk-flex <?php echo mytheme_option( 'hlogoloc' ); ?> <?php echo mytheme_option('htextaligntitle'); ?>">
            <?php if (trim(mytheme_option( 'headerlogo' )) != '') : ?>
            <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>">
                <img src="<?php echo mytheme_option( 'headerlogo' ) ?>" class="" style="width:100%; height:100%; max-width:<?php if (trim(mytheme_option( 'logowidth' )) != '') { echo mytheme_option( 'logowidth' ) . ""; } ?>; max-height:<?php if (trim(mytheme_option( 'logoheight' )) != '') { echo mytheme_option( 'logoheight' ) . ""; } ?>">
            </a>
            <?php else : ?>
            <h1 <?php echo $text_style ?> class="">
                <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" class="uk-link-reset">
                    <?php bloginfo('name'); ?>
                </a>
            </h1>
            <?php endif; ?>
        </figcaption>
        <?php if (mytheme_option( 'hshowdes' )) { ?>
            <figcaption class="uk-overlay-panel <?php echo mytheme_option( 'hoverlayanim' ); ?> uk-flex <?php echo mytheme_option( 'hdesloc' ); ?> <?php echo mytheme_option('htextaligndes'); ?>">
                <?php if (trim(mytheme_option( 'hcustomdes' )) != '') { echo mytheme_option( 'hcustomdes' ); } else { ?>
                <?php if ($description = get_bloginfo('description')) : ?>
                <span <?php echo $text_style ?> class=""><?php echo $description ?></span>
                <?php endif; } ?>
            </figcaption>
        <?php } ?>

      <?php if (mytheme_option('harrownav')) { ?>
        <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous" data-uk-slideshow-item="previous"></a>
        <a href="" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next" data-uk-slideshow-item="next"></a>
       <?php } ?>
        <?php if (mytheme_option('hslidernav') == 'dotnav') : ?>
        <ul class="uk-dotnav uk-dotnav-contrast uk-position-bottom uk-flex-center">
            <?php if (trim(mytheme_option( 'hslideone' )) != '') { ?><li data-uk-slideshow-item="0"><a href=""></a></li> <?php } ?>
            <?php if (trim(mytheme_option( 'hslidetwo' )) != '') { ?><li data-uk-slideshow-item="1"><a href=""></a></li> <?php } ?>
            <?php if (trim(mytheme_option( 'hslidethr' )) != '') { ?> <li data-uk-slideshow-item="2"><a href=""></a></li> <?php } ?>
            <?php if (trim(mytheme_option( 'hslidefr' )) != '') { ?> <li data-uk-slideshow-item="3"><a href=""></a></li> <?php } ?>
        </ul>
        <?php endif; ?>
    </div>
     <div class="<?php if (mytheme_option('hslidermobdis')) { echo 'mobile';} else { echo 'uk-hidden'; } ?>">
            <?php if (trim(get_header_image())!='') : ?>
          <figure class="uk-overlay">
            <img src="<?php echo get_header_image() ?>" class="<?php if (trim(mytheme_option('mobilelogo')) != '' ) { echo 'nodisplay'; } ?>">
            <?php if (trim(mytheme_option('mobilelogo')) != '' ) { ?> <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>"><img src="<?php echo mytheme_option( 'mobilelogo' ) ?>" class="mobile" style="max-width:100%; max-height:100%;"></a> <?php } ?>
            <figcaption class=" <?php if (mytheme_option( 'hremovepadding' )) { echo 'uk-padding-remove'; }; ?> uk-overlay-panel  <?php echo mytheme_option( 'holbg' ); ?> uk-flex <?php echo mytheme_option( 'hlogoloc' ); ?> <?php echo mytheme_option('htextaligntitle'); ?>">
              <?php if (trim(mytheme_option( 'headerlogo' )) != '') : ?>
               <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>"><img src="<?php echo mytheme_option( 'headerlogo' ) ?>" class="<?php if (trim(mytheme_option('mobilelogo')) != '' ) { echo 'nodisplay'; } ?>" style="max-width:100%; max-height:100%; width:<?php if (trim(mytheme_option( 'logowidth' )) != '') { echo mytheme_option( 'logowidth' ) . ""; } ?>; height:<?php if (trim(mytheme_option( 'logoheight' )) != '') { echo mytheme_option( 'logoheight' ) . ""; } ?>"></a>
              <?php else : ?>
              <h1 <?php echo $text_style ?> class="">
                  <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" class="uk-link-reset">
                      <?php bloginfo('name'); ?>
                  </a>
              </h1>
              <?php endif; ?>
              </figcaption>
            <?php if (mytheme_option( 'hshowdes' )) { ?>
            <figcaption class=" <?php if (mytheme_option( 'hremovepadding' )) { echo 'uk-padding-remove'; }; ?> uk-overlay-panel <?php echo mytheme_option( 'holbg' ); ?> uk-flex <?php echo mytheme_option( 'hdesloc' ); ?> <?php echo mytheme_option('htextaligndes'); ?>">
                     <?php if (trim(mytheme_option( 'hcustomdes' )) != '') { echo mytheme_option( 'hcustomdes' ); } else { ?>
                          <?php if ($description = get_bloginfo('description')) : ?>
                          <span <?php echo $text_style ?> class=""><?php echo $description ?></span>
                          <?php endif; } ?>
                        </figcaption> <?php } ?>


          </figure>
        <?php else: ?> <!-- If no header background image and slider disabled -->
             <div class="uk-overlay uk-flex uk-flex-middle" <?php echo $header_style ?>>



            <figcaption class=" <?php if (mytheme_option( 'hremovepadding' )) { echo 'uk-padding-remove'; }; ?> uk-overlay-panel  <?php echo mytheme_option( 'holbg' ); ?> uk-flex <?php echo mytheme_option( 'hlogoloc' ); ?> <?php echo mytheme_option('htextaligntitle'); ?>">

               <?php if (trim(mytheme_option( 'headerlogo' )) != '') : ?>
               <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>"><img src="<?php echo mytheme_option( 'headerlogo' ) ?>" class="<?php if (trim(mytheme_option('mobilelogo')) != '' ) { echo 'nodisplay'; } ?>" style="max-width:100%; max-height:100%; width:<?php if (trim(mytheme_option( 'logowidth' )) != '') { echo mytheme_option( 'logowidth' ) . ""; } ?>; height:<?php if (trim(mytheme_option( 'logoheight' )) != '') { echo mytheme_option( 'logoheight' ) . ""; } ?>"></a>
               <?php if (trim(mytheme_option('mobilelogo')) != '' ) { ?> <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>"><img src="<?php echo mytheme_option( 'mobilelogo' ) ?>" class="mobile" style="max-width:100%; max-height:100%;"></a> <?php } ?>
              <?php else : ?>
              <h1 <?php echo $text_style ?> class="">
                  <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" class="uk-link-reset">
                      <?php bloginfo('name'); ?>
                  </a>
              </h1>
              <?php endif; ?>
              </figcaption>
            <?php if (mytheme_option( 'hshowdes' )) { ?>
            <figcaption class=" <?php if (mytheme_option( 'hremovepadding' )) { echo 'uk-padding-remove'; }; ?> uk-overlay-panel <?php echo mytheme_option( 'holbg' ); ?> uk-flex <?php echo mytheme_option( 'hdesloc' ); ?> <?php echo mytheme_option('htextaligndes'); ?>">
                     <?php if (trim(mytheme_option( 'hcustomdes' )) != '') { echo mytheme_option( 'hcustomdes' ); } else { ?>
                          <?php if ($description = get_bloginfo('description')) : ?>
                          <span <?php echo $text_style ?> class=""><?php echo $description ?></span>
                          <?php endif; } ?>
                        </figcaption> <?php } ?>

            <?php endif; ?>

    </div>
    </div>
    <!--- End of mob alt banner when using slider -->


    <?php } else { ?> <!-- If header background image and slider disabled -->

        <?php if (trim(get_header_image())!='') : ?>
          <figure class="uk-overlay">
            <img src="<?php echo get_header_image() ?>" class="<?php if (trim(mytheme_option('mobilelogo')) != '' ) { echo 'nodisplay'; } ?>">
            <?php if (trim(mytheme_option('mobilelogo')) != '' ) { ?> <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>"><img src="<?php echo mytheme_option( 'mobilelogo' ) ?>" class="mobile" style="max-width:100%; max-height:100%;"></a> <?php } ?>
            <figcaption class=" <?php if (mytheme_option( 'hremovepadding' )) { echo 'uk-padding-remove'; }; ?> uk-overlay-panel  <?php echo mytheme_option( 'holbg' ); ?>  uk-flex <?php echo mytheme_option( 'hlogoloc' ); ?> <?php echo mytheme_option('htextaligntitle'); ?>">
              <?php if (trim(mytheme_option( 'headerlogo' )) != '') : ?>
               <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>"><img src="<?php echo mytheme_option( 'headerlogo' ) ?>" class="<?php if (trim(mytheme_option('mobilelogo')) != '' ) { echo 'nodisplay'; } ?>" style="max-width:100%; max-height:100%; width:<?php if (trim(mytheme_option( 'logowidth' )) != '') { echo mytheme_option( 'logowidth' ) . ""; } ?>; height:<?php if (trim(mytheme_option( 'logoheight' )) != '') { echo mytheme_option( 'logoheight' ) . ""; } ?>"></a>
              <?php else : ?>
              <h1 <?php echo $text_style ?> class="">
                  <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" class="uk-link-reset">
                      <?php bloginfo('name'); ?>
                  </a>
              </h1>
              <?php endif; ?>
              </figcaption>
            <?php if (mytheme_option( 'hshowdes' )) { ?>
            <figcaption class=" <?php if (mytheme_option( 'hremovepadding' )) { echo 'uk-padding-remove'; }; ?> uk-overlay-panel <?php echo mytheme_option( 'holbg' ); ?> uk-flex <?php echo mytheme_option( 'hdesloc' ); ?> <?php echo mytheme_option('htextaligndes'); ?>">
                     <?php if (trim(mytheme_option( 'hcustomdes' )) != '') { echo mytheme_option( 'hcustomdes' ); } else { ?>
                          <?php if ($description = get_bloginfo('description')) : ?>
                          <span <?php echo $text_style ?> class=""><?php echo $description ?></span>
                          <?php endif; } ?>
                        </figcaption> <?php } ?>


          </figure>
        <?php else: ?> <!-- If no header background image and slider disabled -->
             <div class="uk-overlay uk-flex uk-flex-middle" <?php echo $header_style ?>>



            <figcaption class=" <?php if (mytheme_option( 'hremovepadding' )) { echo 'uk-padding-remove'; }; ?> uk-overlay-panel  <?php echo mytheme_option( 'holbg' ); ?> uk-flex <?php echo mytheme_option( 'hlogoloc' ); ?> <?php echo mytheme_option('htextaligntitle'); ?>">

               <?php if (trim(mytheme_option( 'headerlogo' )) != '') : ?>
               <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>"><img src="<?php echo mytheme_option( 'headerlogo' ) ?>" class="<?php if (trim(mytheme_option('mobilelogo')) != '' ) { echo 'nodisplay'; } ?>" style="max-width:100%; max-height:100%; width:<?php if (trim(mytheme_option( 'logowidth' )) != '') { echo mytheme_option( 'logowidth' ) . ""; } ?>; height:<?php if (trim(mytheme_option( 'logoheight' )) != '') { echo mytheme_option( 'logoheight' ) . ""; } ?>"></a>
               <?php if (trim(mytheme_option('mobilelogo')) != '' ) { ?> <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>"><img src="<?php echo mytheme_option( 'mobilelogo' ) ?>" class="mobile" style="max-width:100%; max-height:100%;"></a> <?php } ?>
              <?php else : ?>
              <h1 <?php echo $text_style ?> class="">
                  <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" class="uk-link-reset">
                      <?php bloginfo('name'); ?>
                  </a>
              </h1>
              <?php endif; ?>
              </figcaption>
            <?php if (mytheme_option( 'hshowdes' )) { ?>
            <figcaption class=" <?php if (mytheme_option( 'hremovepadding' )) { echo 'uk-padding-remove'; }; ?> uk-overlay-panel <?php echo mytheme_option( 'holbg' ); ?> uk-flex <?php echo mytheme_option( 'hdesloc' ); ?> <?php echo mytheme_option('htextaligndes'); ?>">
                     <?php if (trim(mytheme_option( 'hcustomdes' )) != '') { echo mytheme_option( 'hcustomdes' ); } else { ?>
                          <?php if ($description = get_bloginfo('description')) : ?>
                          <span <?php echo $text_style ?> class=""><?php echo $description ?></span>
                          <?php endif; } ?>
                        </figcaption> <?php } ?>

            <?php endif; ?>
    <?php } ?>

</div>

</header>
<?php } ?>
