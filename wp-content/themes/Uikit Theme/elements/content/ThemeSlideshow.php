

<?php if (mytheme_option( 'header' )) { ?>
<div id="slider" class="">
  <?php } else { ?>
<div  id="slider" class="">
  <?php } ?>
  <div class="<?php if ((mytheme_option('contentwidth')) && (!mytheme_option('sliderfullwidth'))) { echo 'uk-content-width'; } ?>" style="">
  <div class="uk-slidenav-position <?php echo mytheme_option('overlaydisp'); ?>" data-uk-slideshow="{ <?php if (mytheme_option('kenburns')) { echo 'kenburns:true,'; } ?> <?php if (mytheme_option('autoplay')) { echo 'autoplay:true,'; } ?> <?php echo mytheme_option('anim'); ?> }">
    <ul class="uk-slideshow">
        <?php if (trim(mytheme_option( 'slideone' )) != '') { ?>
        <li>

              <img src="<?php echo mytheme_option( 'slideone' ); ?>">
              <?php if (mytheme_option( 'overlayone' )) { ?>
              <figcaption class="uk-overlay-panel <?php echo mytheme_option( 'olbgone' ); ?> <?php echo mytheme_option( 'overlayanimone' ); ?> uk-flex <?php echo mytheme_option( 'overlaylocone' ); ?> <?php echo mytheme_option('textalignone'); ?>">
              <div class="slideshow-content"><?php echo mytheme_option( 'overlaydesone' ); ?></div>
              </figcaption>
              <?php } ?>


        </li>
        <?php }
        if (trim(mytheme_option( 'slidetwo' )) != '') { ?>
        <li>

              <img src="<?php echo mytheme_option( 'slidetwo' ); ?>">
              <?php if (mytheme_option( 'overlaytwo' )) { ?>
              <figcaption class="uk-overlay-panel <?php echo mytheme_option( 'olbgtwo' ); ?> <?php echo mytheme_option( 'overlayanimtwo' ); ?> uk-flex <?php echo mytheme_option( 'overlayloctwo' ); ?> <?php echo mytheme_option('textaligntwo'); ?>">
              <div class="slideshow-content"><?php echo mytheme_option( 'overlaydestwo' ); ?></div>
              </figcaption>
              <?php } ?>

        </li>
        <?php }
        if (trim(mytheme_option( 'slidethr' )) != '') { ?>
        <li>

              <img src="<?php echo mytheme_option( 'slidethr' ); ?>">
              <?php if (mytheme_option( 'overlaythr' )) { ?>
              <figcaption class="uk-overlay-panel <?php echo mytheme_option( 'olbgthr' ); ?> <?php echo mytheme_option( 'overlayanimthr' ); ?> uk-flex <?php echo mytheme_option( 'overlaylocthr' ); ?> <?php echo mytheme_option('textalignthr'); ?>">
              <div class="slideshow-content"><?php echo mytheme_option( 'overlaydesthr' ); ?></div>
              </figcaption>
              <?php } ?>

        </li>
        <?php }
        if (trim(mytheme_option( 'slidefr' )) != '') { ?>
        <li>

              <img src="<?php echo mytheme_option( 'slidefr' ); ?>">
              <?php if (mytheme_option( 'overlayfr' )) { ?>
              <figcaption class="uk-overlay-panel <?php echo mytheme_option( 'olbgfr' ); ?> <?php echo mytheme_option( 'overlayanimfr' ); ?> uk-flex <?php echo mytheme_option( 'overlaylocfr' ); ?> <?php echo mytheme_option('textalignfr'); ?>">
              <div class="slideshow-content"><?php echo mytheme_option( 'overlaydesfr' ); ?></div>
              </figcaption>
              <?php } ?>

        </li>
        <?php } ?>
      </ul>
      <?php if (mytheme_option('arrownav')) { ?>
        <a href="" class="uk-slidenav uk-slidenav-previous" data-uk-slideshow-item="previous"></a>
        <a href="" class="uk-slidenav uk-slidenav-next" data-uk-slideshow-item="next"></a>
       <?php } ?>
        <?php if (mytheme_option('slidernav') == 'dotnav') : ?>
        <ul class="uk-dotnav uk-dotnav-contrast uk-position-bottom uk-flex-center">
            <?php if (trim(mytheme_option( 'slideone' )) != '') { ?><li data-uk-slideshow-item="0"><a href=""></a></li> <?php } ?>
            <?php if (trim(mytheme_option( 'slidetwo' )) != '') { ?><li data-uk-slideshow-item="1"><a href=""></a></li> <?php } ?>
            <?php if (trim(mytheme_option( 'slidethr' )) != '') { ?> <li data-uk-slideshow-item="2"><a href=""></a></li> <?php } ?>
            <?php if (trim(mytheme_option( 'slidefr' )) != '') { ?> <li data-uk-slideshow-item="3"><a href=""></a></li> <?php } ?>
        </ul>
        <?php endif; ?>
    </div>
</div>
</div>
