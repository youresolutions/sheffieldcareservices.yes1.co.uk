<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * TODO Categories: only print if any set
 */
?>


<article id="post-<?php the_ID(); ?>" <?php post_class(array('uk-article uk-container-center')); ?>>
    <?php if (is_single()) {
        the_title('<h1 class="uk-article-title">', '</h1>');
    } else {
        the_title('<h1 class="uk-article-title"><a href="' . esc_url(get_permalink()) . '" class="uk-link-reset" rel="bookmark">', '</a></h1>');
    }
    ?>
    <?php if (is_user_logged_in()) { ?>
    <p class="uk-article-meta">
        <?php printf(
            '<span class="nst-entry-time uk-margin-small-left"><i class="uk-icon-clock-o"></i> <time datetime="%1$s">%2$s</time></span>',
            esc_attr(get_the_date('c')),
            esc_html(get_the_date())
        ); ?>
        <span class="nst-category-list uk-margin-small-left uk-link-reset">
            <i class="uk-icon-ticket"></i> <?php echo get_the_category_list(', '); ?>
        </span>
        <?php if (!post_password_required() && (comments_open() || get_comments_number())) : ?>
            <span class="nst-comments uk-margin-small-left uk-link-reset">
                <?php comments_popup_link(
                    '<i class="uk-icon-comment"></i> ' . __('Leave a comment'), '<i class="uk-icon-comment"></i> 1', '<i class="uk-icon-comment"></i> %'); ?>
            </span>
        <?php endif; ?>
        <?php edit_post_link('<i class="uk-icon-edit"></i> ' . __('Edit'), '<span class="nst-edit-link uk-margin-small-left uk-link-reset">', '</span>'); ?>
        <?php if (is_single()) : ?>
            <br/ >
            <?php the_tags('<span class="nst-tag-list uk-link-reset"><i class="uk-icon-tag"></i> ', ', ', '</span>'); ?>
        <?php endif; ?>
    </p>
    <?php } ?>
    <hr class="uk-article-divider">
    <?php if (has_post_thumbnail()) { ?>
    <div class="uk-grid">
    <div class="uk-width-small-1-3">
        <div class="uk-text-center">
            <?php global $theme;
            $theme->helpers->postThumbnail();
            $prices = get_post_custom_values('price', $post_id);
                if($prices) {
                foreach($prices as $price)
                    echo '<p class="price"><strong>Price: </strong>'.$price.'</p>'; }
             ?>
        </div>
    </div>
    <div class="uk-width-small-2-3">
    <?php } ?>

    <?php if (is_search()) : ?>
        <div class="nst-entry-summary">
            <?php the_excerpt(); ?>
        </div><!-- .entry-summary -->
    <?php else : ?>
        <div class="nst-entry-content">
            <?php
            the_content('Continue reading <span class="meta-nav">&rarr;</span>');
            //            wp_link_pages( array(
            //                'before'      => '<div class="page-links"><span class="page-links-title">Pages</span>',
            //                'after'       => '</div>',
            //                'link_before' => '<span>',
            //                'link_after'  => '</span>',
            //            ) );
            ?>
        </div><!-- .entry-content -->
    <?php endif; ?>
    <?php if (has_post_thumbnail()) { ?>
    </div>
    </div>
    <?php } ?>

    <?php ?>
    <!-- Extras Slider -->
    <?php if (is_single()) { ?>
    <div class="extras" style="padding-top:20px;">
        <?php
        $orig_post = $post;
        global $post;
        $tag_slugs = wp_get_post_tags( $post->ID, array( 'fields' => 'slugs' ) );
        if (!in_array("extras", $tag_slugs)) {


            $args=array(
                'tag' => 'extras',
                'post__not_in' => array($post->ID),
                'posts_per_page'=>20, // Number of related posts to display.
                'caller_get_posts'=>1
            );

        $my_query = new wp_query( $args );
        /*if ($my_query->post_count < 5) {$postcount = $my_query->post_count;;} else { $postcount = '4'; };*/
        $postcount = $my_query->post_count;

        if ($postcount > 0) {

        echo '<h3>Why not treat yourself to something a bit extra?</h3>';

         echo '<div style="padding-top:20px;"><div class="uk-slidenav-position" data-uk-slider="{center:false,infinite:false}"><div class="uk-slider-container"><ul class="uk-slider">';




        while( $my_query->have_posts() ) {
            $my_query->the_post();


        ?>
             <li class='uk-width-small-1-4'>
                 <div class="uk-flex uk-flex-center">
                     <a class="uk-thumbnail" style="border:none;" href="<?php the_permalink()?>"><?php the_post_thumbnail();?>
                         <div class='uk-thumbnail-caption'>
                             <p class="" style="color:#444"><?php the_title();?></p>
                             <?php $extraprices = get_post_custom_values('price', $post_id); if($extraprices) { foreach($extraprices as $extraprice)  echo '<p class="price"><strong>Price: </strong>'.$extraprice.'</p>'; } ?>
                         </div>
                     </a>
                 </div>
             </li>


        <?php }

        echo '</ul></div>';
        if (($postcount) > 4) { echo '<a href="" class="uk-slidenav uk-slidenav-previous" data-uk-slider-item="previous"></a><a href="" class="uk-slidenav uk-slidenav-next" data-uk-slider-item="next"></a>'; }
        echo '</div></div>';
        }
        $post = $orig_post;
        wp_reset_query();
        ?>
    </div>
    <?php } } ?>

    <!-- Related Products Slider -->
    <?php if (is_single()) { ?>
    <div class="relatedproducts" style="padding-top:20px;">

    <?php
        $tags = wp_get_post_tags($post->ID);
        if ($tags) {
            $tag_ids = array();
        foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
            $args=array(
                'tag__in' => $tag_ids,
                'post__not_in' => array($post->ID),
                'posts_per_page'=>20, // Number of related posts to display.
                'caller_get_posts'=>1
            );

        $my_query = new wp_query( $args );
        /*if ($my_query->post_count < 5) {$postcount = $my_query->post_count;;} else { $postcount = '4'; };*/
        $postcount = $my_query->post_count;

        if ($postcount > 0) {

        echo '<h3>Related Products</h3>';

         echo '<div style="padding-top:20px;"><div class="uk-slidenav-position" data-uk-slider="{center:false,infinite:false}"><div class="uk-slider-container"><ul class="uk-slider">';




        while( $my_query->have_posts() ) {
            $my_query->the_post();


        ?>
             <li class='uk-width-small-1-4'>
                 <div class="uk-flex uk-flex-center">
                     <a class="uk-thumbnail" style="border:none;" href="<?php the_permalink()?>"><?php the_post_thumbnail();?>
                         <div class='uk-thumbnail-caption'>
                             <p class="" style="color:#444"><?php the_title();?></p>
                             <?php $relatedprices = get_post_custom_values('price', $post_id); if($relatedprices) { foreach($relatedprices as $relatedprice)  echo '<p class="price"><strong>Price: </strong>'.$relatedprice.'</p>'; } ?>
                         </div>
                     </a>
                 </div>
             </li>


        <?php }
        }
        echo '</ul></div>';
        if (($postcount) > 4) { echo '<a href="" class="uk-slidenav uk-slidenav-previous" data-uk-slider-item="previous"></a><a href="" class="uk-slidenav uk-slidenav-next" data-uk-slider-item="next"></a>'; }
        echo '</div></div>';
        }
        $post = $orig_post;
        wp_reset_query();
        ?>
    </div>
    <?php } ?>


</article>
