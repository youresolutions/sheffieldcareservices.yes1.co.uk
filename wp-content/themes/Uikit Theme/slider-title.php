<?php
/*
Template Name: Page title in page slider (4th slide overlay)
*/

get_header();
get_template_part('elements/base/header');
get_template_part('elements/base/navigation');

$slides = get_post_custom_values('slide', get_the_ID() );


?>

<div  id="slider" class="">

  <div class="<?php if ((mytheme_option('contentwidth')) && (!mytheme_option('sliderfullwidth'))) { echo 'uk-content-width'; } ?>" style="">
  <div class="uk-slidenav-position <?php echo mytheme_option('overlaydisp'); ?>" data-uk-slideshow="{ <?php if (mytheme_option('kenburns')) { echo 'kenburns:true,'; } ?> <?php if (mytheme_option('autoplay')) { echo 'autoplay:true,'; } ?> <?php echo mytheme_option('anim'); ?> }">

        <?php  if(!empty(array_filter($slides))) {
        ?>	<ul class="uk-slideshow"> <?php
                foreach($slides as $slide) { ?>
        <li>

              <img src="<?php echo $slide; ?>">
              <figcaption class="uk-overlay-panel <?php echo mytheme_option( 'olbgfr' ); ?> <?php echo mytheme_option( 'overlayanimfr' ); ?> uk-flex <?php echo mytheme_option( 'overlaylocfr' ); ?> <?php echo mytheme_option('textalignfr'); ?>">

              <div class="slideshow-content-slider-title"><h1><?php echo get_the_title(); ?></h1></div>
              </figcaption>


        </li>
        <?php } ?>
        </ul>
        <?php  } else { ?>
       	<div class="no-img"></div>

      <?php} if (mytheme_option('arrownav')) { ?>
        <a href="" class="uk-slidenav uk-slidenav-previous" data-uk-slideshow-item="previous"></a>
        <a href="" class="uk-slidenav uk-slidenav-next" data-uk-slideshow-item="next"></a>
       <?php } ?>
        <?php if (mytheme_option('slidernav') == 'dotnav') : ?>
        <ul class="uk-dotnav uk-dotnav-contrast uk-position-bottom uk-flex-center">
        	<?php  if($slides) {
        		$count=0;
                foreach($slides as $slide) { ?>
		        	<li data-uk-slideshow-item="<?php echo $count; ?>"><a href=""></a></li>
        		<?php $count++;
        		} } ?>
        </ul>
        <?php endif; ?>
    </div>
</div>

</div>



<?php get_template_part('elements/base/precontent'); ?>

	<div id="primary" class="content-area">
		<div id="content" class="<?php if (mytheme_option('pagepadding')) { echo 'uk-container'; } ?> uk-margin-bottom uk-margin-top" role="main">

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
						<div class="entry-thumbnail">
							<?php the_post_thumbnail(); ?>
						</div>
						<?php endif; ?>

						<noscript><h1><?php echo get_the_title(); ?></h1></noscript>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					</div><!-- .entry-content -->

					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-meta -->
				</article><!-- #post -->

			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php
get_template_part('elements/base/postcontent');
get_template_part('elements/base/footer'); ?>
<?php get_footer(); ?>
