<?php
/**
 * Default template for sidebar
 */
?>

<?php if (is_active_sidebar('sidebar-main')) : ?>
    <aside id="first-sidebar" class="widget-container uk-active">
        <?php dynamic_sidebar('sidebar-main'); ?>
    </aside>
<?php endif; ?>
