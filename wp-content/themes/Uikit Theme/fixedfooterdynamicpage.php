<?php
/*
Template Name: Fixed Footer on Dynamic Content Pages Template
*/
get_header();
get_template_part('elements/base/header');
get_template_part('elements/base/navigation');
get_template_part('elements/base/precontent');


?>
<section id="" class="">
	<div id="primary" class="content-area">
		<div id="custompage" class="<?php if (mytheme_option('pagepadding')) { echo 'uk-container'; } ?> uk-margin-bottom uk-margin-top" role="main">

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">
						<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
						<div class="entry-thumbnail">
							<?php the_post_thumbnail(); ?>
						</div>
						<?php endif; ?>

						<!--<h1 class="entry-title"></h1>-->
					</header><!-- .entry-header -->

					<div class="entry-content">
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					</div><!-- .entry-content -->

					<footer class="entry-meta">
						<?php edit_post_link( __( 'Edit' ), '<span class="edit-link">', '</span>' ); ?>
					</footer><!-- .entry-meta -->
				</article><!-- #post -->

			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->
</section>
<?php
get_template_part('elements/base/postcontent');
get_template_part('elements/base/footer'); ?>

<?php get_footer(); ?>
