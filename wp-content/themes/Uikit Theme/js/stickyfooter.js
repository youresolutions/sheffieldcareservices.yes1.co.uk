

//Initial load of page
//jQuery(window).load(setTimeout(sizeContent),stickyfooter_vars.home);

//Page Fully Loaded
jQuery(window).on('load',ContentSize).each(function() {
  if(this.complete) jQuery(this).load();
});

//Every resize of window
jQuery(window).resize(ContentSize);



//Dynamically assign height
function preparecontent () {

}

function ContentSize() {
    jQuery("#wrapper").removeClass("stickyfooter");
    var winWidth = navigator.userAgent.indexOf('AppleWebKit/') > -1 ? jQuery(window).width() : window.innerWidth;
    var sidebarheight = jQuery("#first-sidebar").outerHeight(true) + jQuery("#second-sidebar").outerHeight(true);
    var sidebars;
    if ((stickyfooter_vars.mobile) < winWidth) { sidebars = 0 } else { sidebars = sidebarheight }
    var winHeight = jQuery(window).innerHeight();
    var contentHeight = jQuery("#header").height() + jQuery("#nav").height() + jQuery("#slider").height() + jQuery("#socialmedia").height() + jQuery("#custompage").height() + jQuery("#footer").height() + sidebars;
    var newHeight = jQuery("html").height() - jQuery("#header").height() - jQuery("#nav").height() - jQuery("#slider").height() - jQuery("#socialmedia").height() - jQuery("#footer").height() - sidebars;
    if (stickyfooter_vars.socialmediamob) {
        contentHeight = contentHeight - jQuery("#socialmedia").height();
        newHeight = newHeight + jQuery("#socialmedia").height();
    }
    newHeight = newHeight + "px";
    if (winHeight > contentHeight) {
    jQuery("#maincontent").css("height", newHeight);
    } else {jQuery("#maincontent").css("height", "100%");}
    jQuery("#footer").css("visibility", "visible");
    jQuery("#footer").addClass('uk-animation-fade-fast');

};
