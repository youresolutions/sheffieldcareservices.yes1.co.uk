<?php
/**
 * Default template for sidebar
 */
?>

<?php if (is_active_sidebar('sidebar-shop')) : ?>
    <div id="shop-sidebar" class="widget-area" role="complementary" style="padding:10px;">        
        <?php dynamic_sidebar('sidebar-shop'); ?>
    </div>
<?php endif; ?>
