<?php
/**
 * Default template for sidebar
 */
?>

<?php if (is_active_sidebar('sidebar-secondary')) : ?>
    <aside id="second-sidebar" class="widget-container uk-active">
        <?php dynamic_sidebar('sidebar-secondary'); ?>
    </aside>
<?php endif; ?>
