<?php
/**
 * template for footer sidebar
 */

$sidebar_id = 'sidebar-footer';
?>
<?php if (is_active_sidebar($sidebar_id)) : ?>
    <section class="nst-sidebar-horizontal uk-margin-bottom uk-margin-top">
        <div class="uk-grid uk-grid-medium" data-uk-grid-margin data-uk-grid-match="{target:'.uk-panel'}">
            <?php dynamic_sidebar($sidebar_id); ?>
        </div>
    </section>
<?php endif; ?>
