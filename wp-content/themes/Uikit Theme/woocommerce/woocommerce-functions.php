<?php

/** Woocommerce **/

remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10);

add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);

function my_theme_wrapper_start() {
  get_template_part('elements/base/header');
  get_template_part('elements/base/navigation');
  echo '<section id="main" class=""><div id="contentwrapper" class="';
    if ((mytheme_option('contentwidth')) && (mytheme_option('pagepadding'))) { echo 'uk-content-width'; }
    echo'">';
    echo '<div class="uk-grid" style="margin-left:0px; margin-right:0px;">';
    if (is_shop() || is_product_category()) {
      if (!mytheme_option('wooshopsidebar'))  {
        if (mytheme_option('woodoublesidebars'))  {
        echo '<div class="uk-width-twosidebar uk-sidebar-bg uk-sidebar-border-right';
        if (!mytheme_option( 'mobilesidebar' )) { echo ' nodisplay'; };
        if (mytheme_option( 'sidebarlast' )) { echo ' showlastleftsidebar'; };
        if (mytheme_option( 'centersidebarmob' )) { echo ' centersidebar'; };
        echo '" style="padding-right:0px; padding-left:0px;">';
        get_sidebar( 'shop' );
        echo '</div>';
        echo '<div id="maincontent" class="uk-width-twosidebarcontent" style="padding-left:0px">';}
        else {
        echo '<div class="uk-width-onesidebar uk-sidebar-bg uk-sidebar-border-right';
        if (!mytheme_option( 'mobilesidebar' )) { echo ' nodisplay'; };
        if (mytheme_option( 'sidebarlast' )) { echo ' showlastleftsidebar'; };
        if (mytheme_option( 'centersidebarmob' )) { echo ' centersidebar'; };
        echo '" style="padding-right:0px; padding-left:0px;">';
        get_sidebar( 'shop' );
        echo '</div>';
        echo '<div id="maincontent" class="uk-width-onesidebarcontent" style="padding-left:0px">';} }
       else {
        echo '<div id="maincontent" class="uk-width-1-1" style="padding-left:0px">';
      }
    }
    else
    {

      if (!mytheme_option('wooproductsidebar'))  {
          if (mytheme_option('woodoublesidebars')) {
        echo '<div class="uk-width-twosidebar uk-sidebar-bg uk-sidebar-border-right';
        if (!mytheme_option( 'mobilesidebar' )) { echo ' nodisplay'; };
        if (mytheme_option( 'sidebarlast' )) { echo ' showlastleftsidebar'; };
        if (mytheme_option( 'centersidebarmob' )) { echo ' centersidebar'; };
        echo '" style="padding-right:0px; padding-left:0px;">';
        get_sidebar( 'product' );
        echo '</div>';
        echo '<div id="maincontent" class="uk-width-twosidebarcontent" style="padding-left:0px">';} else {
        echo '<div class="uk-width-onesidebar uk-sidebar-bg uk-sidebar-border-right';
        if (!mytheme_option( 'mobilesidebar' )) { echo ' nodisplay'; };
        if (mytheme_option( 'sidebarlast' )) { echo ' showlastleftsidebar'; };
        if (mytheme_option( 'centersidebarmob' )) { echo ' centersidebar'; };
        echo '" style="padding-right:0px; padding-left:0px;">';
        get_sidebar( 'product' );
        echo '</div>';
        echo '<div id="maincontent" class="uk-width-onesidebarcontent" style="padding-left:0px">';} }
      else {
        echo '<div id="maincontent" class="uk-width-1-1" style="padding-left:0px">';
      }

    }
    echo '<div id="primary" class="content-area">
		<div id="content" class="';
    if (mytheme_option('pagepadding')) { echo 'uk-container'; }
  echo ' uk-margin-bottom uk-margin-top" role="main">';
}

function my_theme_wrapper_end() {
  echo '</div></div></div>';
  if (!mytheme_option('wooshopsidebar') && mytheme_option('woodoublesidebars') && (is_shop() || is_product_category())) {
      ?>
      <div class="uk-width-twosidebar uk-sidebar-bg uk-sidebar-border-left <?php if (!mytheme_option( 'mobilesidebar' )) { echo 'nodisplay'; } ?> <?php if (mytheme_option( 'sidebarlast' )) { echo 'showlastrightsidebar'; } ?> <?php if (mytheme_option( 'centersidebarmob' )) { echo 'centersidebar'; } ?>" style="padding-left:0px; padding-right:0px;">
                           <?php get_sidebar( 'secondaryshop' ); ?>
                    </div>
     <?php
    }
  if (!mytheme_option('wooproductsidebar') && mytheme_option('woodoublesidebars') && !(is_shop() || is_product_category())) {
          ?>  <div class="uk-width-twosidebar uk-sidebar-bg uk-sidebar-border-left <?php if (!mytheme_option( 'mobilesidebar' )) { echo 'nodisplay'; } ?> <?php if (mytheme_option( 'sidebarlast' )) { echo 'showlastrightsidebar'; } ?> <?php if (mytheme_option( 'centersidebarmob' )) { echo 'centersidebar'; } ?>" style="padding-left:0px; padding-right:0px;">
                           <?php get_sidebar( 'secondaryproduct' ); ?>
                    </div>  <?php
  }
  echo '</div></div></section>';
  get_template_part('elements/base/footer');
}


if ( ! function_exists( 'uikit_get_cart_button_html' ) ) {
	function uikit_get_cart_button_html() {

		if (class_exists('Woocommerce')) {
			global $woocommerce;
      $amount = WC()->cart->cart_contents_total+WC()->cart->tax_total;
        $amount = number_format((float)$amount, 2, '.', '');
					?><a class="uk-button-cart" href="#" title="<?php _e( 'View your shopping cart' ); ?>"><i class="uk-icon-shopping-cart"></i>&nbsp;<?php echo sprintf (_n( '%d item', '%d items', WC()->cart->cart_contents_count ), WC()->cart->cart_contents_count ); ?> - <?php echo get_woocommerce_currency_symbol().$amount; ?></a>
<?php

		}

	}
}
remove_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );
add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );
function woocommerce_header_add_to_cart_fragment( $fragments ) {
	ob_start();
  $amount = WC()->cart->cart_contents_total+WC()->cart->tax_total;
  $amount = number_format((float)$amount, 2, '.', '');
	?>
	<a class="uk-button-cart" href="#" title="<?php _e( 'View your shopping cart' ); ?>"><i class="uk-icon-shopping-cart"></i>&nbsp;<?php echo sprintf (_n( '%d item', '%d items', WC()->cart->cart_contents_count ), WC()->cart->cart_contents_count ); ?> - <?php echo get_woocommerce_currency_symbol().$amount; ?></a>
	<?php

	$fragments['a.uk-button-cart'] = ob_get_clean();

	return $fragments;
}

add_action('wp_head','hook_cart_css');

function hook_cart_css() {

  if ((mytheme_option('stickynav')) && (mytheme_option('stickysocial'))) {

	echo '<style> .mobile .uk-dropdown-bottom { top:110px !important; } @media screen and (max-width: 550px) { .mobile .uk-dropdown-bottom { top:90px !important; } } @media screen and (max-width: 400px) { .mobile .uk-dropdown-bottom { top:70px !important; } } </style>';

  }
}


/**<?php echo WC()->cart->get_cart_url(); ?>**/

add_action( 'widgets_init', 'new_woocommerce_widgets', 15 );

function new_woocommerce_widgets() {

    include_once( 'class-wc-widget-cart-navbar.php' );

    register_widget( 'WC_Widget_Cart_Navbar' );

        include_once( 'class-wc-widget-recently-viewed-never-hidden.php' );

    register_widget( 'WC_Widget_Recently_Viewed_Never_Hidden' );
  }

/** Change sort text **/

add_filter( 'gettext', 'theme_sort_change', 20, 3 );
function theme_sort_change( $translated_text, $text, $domain ) {

    if ( is_woocommerce() ) {

        switch ( $translated_text ) {

            case 'Default sorting' :

                $translated_text = __( 'Featured', 'theme_text_domain' );
                break;
        }

    }

    return $translated_text;
}

/** Product Slider Shortcode **/

function uikit_featured_products($atts) {
   ob_start();
   extract(shortcode_atts(array(
      'products' => 6,
      'columns'  => 4,
   ), $atts));
   $meta_query  = WC()->query->get_meta_query();
   $tax_query   = WC()->query->get_tax_query();
   $tax_query[] = array(
			'taxonomy' => 'product_visibility',
			'field'    => 'name',
			'terms'    => 'featured',
			'operator' => 'IN',
		);

    $args = array(
        'post_type'   =>  'product',
        'stock'       =>  1,
        'showposts'   =>  $products,
        'orderby'     =>  'date',
        'order'       =>  'DESC',
        'meta_query'  =>  $meta_query,
        'tax_query'           => $tax_query,
    );
    if ($columns <= 5) {$grid = $columns;} else {$grid = 5;};
    $loop = new WP_Query( $args ); ?>
    <?php if ( $loop->have_posts() ) : ?>
    <div id="featured" style="padding-top:20px;"><div class="uk-slidenav-position" data-uk-slider="{center:false,infinite:false}"><div class="uk-slider-container"><ul class="uk-slider uk-grid">
    <?php while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>

        <li class='uk-text-center uk-width-small-1-<?php echo $grid ?>'>
            <?php
                if ( has_post_thumbnail( $loop->post->ID ) )
                    echo '<div class="centeralignmob"><a style="display: block;" href="'.get_permalink().'">'.get_the_post_thumbnail( $loop->post->ID, 'shop_catalog' ).'</a></div>';
                else
                    echo '<div class="centeralignmob"><img src="' . woocommerce_placeholder_img_src() . '" alt="Placeholder" width="100%" height="100%" /></div>';
            ?>
            <div style="width=100%; height:100%; clear: both; text-align:center;">
            <h3 class="fslidertitle"><a href="<?php echo get_permalink() ?>"><?php the_title(); ?></a></h3>
            <span class="featuredprice"><?php echo $product->get_price_html(); ?></span>
            <div class="featuredbuybutton" style=""><?php woocommerce_template_loop_add_to_cart( $loop->post, $product ); ?> </div>
            </div>
        </li>

<?php endwhile; ?>
        </ul></div>
        <?php if (($products) > $columns) { echo '<a href="" class="uk-slidenav uk-slidenav-previous" data-uk-slider-item="previous"></a><a href="" class="uk-slidenav uk-slidenav-next" data-uk-slider-item="next"></a>'; } else {
            { echo '<a href="" class="uk-slidenav uk-slidenav-previous displaynav" data-uk-slider-item="previous"></a><a href="" class="uk-slidenav uk-slidenav-next displaynav" data-uk-slider-item="next"></a>'; }
        } ?>
        </div></div>
        <?php endif; ?>
<?php $myproducts = ob_get_clean();
wp_reset_query();
return $myproducts;
}

add_shortcode('uikitfeaturedproducts', 'uikit_featured_products');

if (mytheme_option('wooturnoff')) {

add_action('wp_head','hook_wc');

function hook_wc() {

    echo '<style> .featuredbuybutton,#basket { display:none !important }; </style>';
}

add_action('wp_footer','hook_notify');

function hook_notify() {
    if (trim(mytheme_option( 'woowarningmsg' )) != '') {
    echo '<div id="shopinfo" class="';
    if (!mytheme_option( 'fullscreen' )) { echo 'uk-custom-width'; }
    echo '">';
    echo '<div class="woo-warning woo-warning-loc uk-alert uk-alert-danger" data-uk-alert>';
    echo '<a href class="uk-alert-close uk-close"></a><p>';
    echo mytheme_option('woowarningmsg');
    echo '</p>';
    echo '</div>';
    echo '</div>';
    }

}

function remove_loop_button(){
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
}
add_action('init','remove_loop_button');

}
