<?php

/**
 * Manages all sidebar-related modifications.
 */
class ThemeSidebars
{

    public function __construct()
    {
        // register main sidebar
        register_sidebar(array(
            'name'          => 'Sidebar',
            'id'            => 'sidebar-main',
            'description'   => 'Main Sidebar. When double sidebars are enabled, this is the left one.',
            'before_widget' => '<div id="%1$s" class="%2$s uk-panel widget-container">',
            'after_widget'  => '</div>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>'
        ));

if (mytheme_option('twosidebars')) {
        register_sidebar(array(
            'name'          => 'Second Sidebar',
            'id'            => 'sidebar-secondary',
            'description'   => 'Main Sidebar on the right side when sidebars on both sides. If double sidebars is diabled, this is not used.',
            'before_widget' => '<div id="%1$s" class="%2$s uk-panel widget-container">',
            'after_widget'  => '</div>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>'
        ));};

        // register footer sidebar
        register_sidebar(array(
            'name'          => 'Footer Sidebar',
            'id'            => 'sidebar-footer',
            'description'   => 'Horizontal Sidebar in the footer',
            'before_widget' => '<div id="%1$s" class="%2$s nst-widget uk-panel footer-panel uk-panel-space">',
            'after_widget'  => '</div></div>',
            'before_title'  => '<div class="uk-panel-title">',
            'after_title'   => '</div>'
        ));

	if (class_exists('woocommerce')){
		register_sidebar( array(
			'name' => __( 'Shop Page Sidebar' ),
			'id' => 'sidebar-shop',
			'before_widget' => '<div id="%1$s" class="%2$s nst-widget uk-panel widget-area">',
			'after_widget' => '</div>',
			'before_title' => '<h2 class="widget-title">',
			'after_title' => '</h2>',
		) );

		register_sidebar( array(
			'name' => __( 'Product Page Sidebar' ),
			'id' => 'sidebar-product',
			'before_widget' => '<div id="%1$s" class="%2$s nst-widget uk-panel widget-area">',
			'after_widget' => '</div>',
			'before_title' => '<h2 class="widget-title">',
			'after_title' => '</h2>',
		) );
		register_sidebar( array(
			'name' => __( 'Right Shop Page Sidebar' ),
			'id' => 'sidebar-secondaryshop',
			'before_widget' => '<div id="%1$s" class="%2$s nst-widget uk-panel widget-area">',
			'after_widget' => '</div>',
			'before_title' => '<h2 class="widget-title">',
			'after_title' => '</h2>',
		) );

		register_sidebar( array(
			'name' => __( 'Right Product Page Sidebar' ),
			'id' => 'sidebar-secondaryproduct',
			'before_widget' => '<div id="%1$s" class="%2$s nst-widget uk-panel widget-area">',
			'after_widget' => '</div>',
			'before_title' => '<h2 class="widget-title">',
			'after_title' => '</h2>',
		) );
	}

        // Add filter for the footer-sidebar
        // Only apply this filter, if not the admin-panel is requested
        if (!is_admin()) {
            add_filter('dynamic_sidebar_params', array($this, 'dynamicFooterParams'));
        }
    }


    /**
     * Filters the params for the sidebars
     *
     * @param array $params The params of the widget
     *
     * @return array The params of the widget
     */
    public function dynamicFooterParams($params)
    {
        // only affect the params of the sidebar-footer
        if ($params[0]['id'] == 'sidebar-footer') {
            // Add a uk-width-medium-1-x class, where x is the number of the widgets within the sidebar
            $class = 'uk-width-medium-1-' . $this->getWidgetsCount('sidebar-footer') . ' uk-width-small-1-' . intval( ($this->getWidgetsCount('sidebar-footer')) / 2);
            $params[0]['before_widget'] = preg_replace('(class=")', 'class="' . $class . '"</div><div class="', $params[0]['before_widget']);
        }

        return $params;
    }

    /**
     * Return the number of widgets within a sidebar
     */
    private function getWidgetsCount($sidebar_id)
    {
        $sidebars_widgets = wp_get_sidebars_widgets();

        return (int)count((array)$sidebars_widgets[$sidebar_id]);
    }

}
