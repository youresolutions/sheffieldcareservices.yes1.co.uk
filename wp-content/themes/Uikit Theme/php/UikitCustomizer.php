<?php

/**
 * Manages theme options in the wordpress customizer
 **/

add_action( 'customize_register', 'uikit_customize_register' );
function uikit_customize_register($wp_customize) {

 $wp_customize->remove_section("colors");

/* General Settings */

$wp_customize->add_section( 'general', array(
    'title'          => __( 'General', 'uikit' ),
    'priority'       => 30,
    'description'    => 'Change any general theme settings.</br></br><a href="http://getuikit.com/docs/customizer.html"><img src="/wp-content/themes/Uikit Theme/customizer.png"></a>',
) );

$wp_customize->add_setting( 'mytheme_options[fullscreen]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'fullscreen', array(
    'settings' => 'mytheme_options[fullscreen]',
    'label'    => __( 'Website fits to the fullscreen width' ),
    'section'  => 'general',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[contentwidth]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'contentwidth', array(
    'settings' => 'mytheme_options[contentwidth]',
    'label'    => __( 'Content is a fixed width even when fullscreen.' ),
    'section'  => 'general',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[Custom_Width]', array(
    'default'        => '1200',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'Custom_Width', array(
    'settings' => 'mytheme_options[Custom_Width]',
    'label'    => __( 'Custom fixed width of the website in pixels.' ),
    'section'  => 'general',
    'type'     => 'text',
) );

$wp_customize->add_setting( 'mytheme_options[pagepadding]', array(
    'default'        => 1,
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'pagepadding', array(
    'settings' => 'mytheme_options[pagepadding]',
    'label'    => __( 'Untick to remove any padding of fixed width from the page content iself.' ),
    'section'  => 'general',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[breadcrumbs]', array(
    'default'        => 0,
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'breadcrumbs', array(
    'settings' => 'mytheme_options[breadcrumbs]',
    'label'    => __( 'Enable breadcrumbs on all pages.' ),
    'section'  => 'general',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[mobiledropdown]', array(
    'default'        => '950',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'mobiledropdown', array(
    'settings' => 'mytheme_options[mobiledropdown]',
    'label'    => __( 'At what mobile width in pixels should the website drop into mobile format.' ),
    'section'  => 'general',
    'type'     => 'text',
) );

$wp_customize->add_setting( 'mytheme_options[scrolltotop]', array(
    'default'        => 0,
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'scrolltotop', array(
    'settings' => 'mytheme_options[scrolltotop]',
    'label'    => __( 'Enable a Scroll to Top button on the site.' ),
    'section'  => 'general',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[scrolltext]', array(
    'default'        => 'Top',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'scrolltext', array(
    'settings' => 'mytheme_options[scrolltext]',
    'label'    => __( 'Set what text the scroll to top button will display.' ),
    'section'  => 'general',
    'type'     => 'text',
) );

$wp_customize->add_setting( 'mytheme_options[scrollcolour]', array(
    'default'        => 'red',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control(
	new WP_Customize_Color_Control(
	$wp_customize,
	'scrollcolour',
	array(
		'label'      => __( 'Scroll to Top button colour', 'uikit' ),
		'section'    => 'general',
		'settings'   => 'mytheme_options[scrollcolour]',
	) )
);

$wp_customize->add_setting( 'mytheme_options[custombgcol]', array(
    'default'        => '#FFFFFF',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control(
	new WP_Customize_Color_Control(
	$wp_customize,
	'custombgcol',
	array(
		'label'      => __( 'Content Area Background Colour', 'uikit' ),
		'section'    => 'general',
		'settings'   => 'mytheme_options[custombgcol]',
	) )
);

$wp_customize->add_setting( 'mytheme_options[enablezoom]', array(
    'default'        => 0,
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'enablezoom', array(
    'settings' => 'mytheme_options[enablezoom]',
    'label'    => __( 'Tick to enable a image zoom feature on post featured images.' ),
    'section'  => 'general',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[enableloading]', array(
    'default'        => 0,
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'enableloading', array(
    'settings' => 'mytheme_options[enableloading]',
    'label'    => __( 'Tick to enable a loading icon while the page is loading.' ),
    'section'  => 'general',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[loadingicon]', array(
    'default'        => '/wp-content/themes/Uikit%20Theme/loading.gif',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
    'sanitize_callback' => 'esc_url_raw',
) );

$wp_customize->add_control(
       new WP_Customize_Image_Control(
           $wp_customize,
           'loadingicon',
           array(
               'label'      => __( 'Upload a custom loading icon to use for your site.', 'uikit' ),
               'section'    => 'general',
               'settings'   => 'mytheme_options[loadingicon]',
           )
       )
   );

/* Logo options in Site Identity */

$wp_customize->add_setting( 'mytheme_options[headerlogo]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
    'sanitize_callback' => 'esc_url_raw',
) );

$wp_customize->add_control(
       new WP_Customize_Image_Control(
           $wp_customize,
           'headerlogo',
           array(
               'label'      => __( 'Upload a logo to use for your website. Displays on the header if enabled, otherwise it will display in the navigation bar.', 'uikit' ),
               'section'    => 'title_tagline',
               'settings'   => 'mytheme_options[headerlogo]',
               'priority'   => '54',
           )
       )
   );

$wp_customize->add_setting( 'mytheme_options[logoheight]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'logoheight', array(
    'settings' => 'mytheme_options[logoheight]',
    'label'    => __( 'Enter the height you want the logo to be.' ),
    'section'  => 'title_tagline',
    'type'     => 'text',
    'priority'   => '54',
) );

$wp_customize->add_setting( 'mytheme_options[logowidth]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'logowidth', array(
    'settings' => 'mytheme_options[logowidth]',
    'label'    => __( 'Enter the width you want the logo to be.' ),
    'section'  => 'title_tagline',
    'type'     => 'text',
    'priority'   => '54',
) );

$wp_customize->add_setting( 'mytheme_options[mobilelogo]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
    'sanitize_callback' => 'esc_url_raw',
) );

$wp_customize->add_control(
       new WP_Customize_Image_Control(
           $wp_customize,
           'mobilelogo',
           array(
               'label'      => __( 'Choose a logo to use on mobile, leave blank if you want to use the same one.', 'uikit' ),
               'section'    => 'title_tagline',
               'settings'   => 'mytheme_options[mobilelogo]',
               'priority'   => '54',
           )
       )
   );

/* Navigation Bar Options */

$wp_customize->add_section( 'navbar', array(
    'title'          => __( 'Navigation Bar', 'uikit' ),
    'priority'       => 30,
    'description'    => 'Change Navigation bar related settings here.',
) );

$wp_customize->add_setting( 'mytheme_options[stickynav]', array(
    'default'        => 1,
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'stickynav', array(
    'settings' => 'mytheme_options[stickynav]',
    'label'    => __( 'Sticky Navigation Bar' ),
    'section'  => 'navbar',
    'type'     => 'checkbox',
) );


$wp_customize->add_setting( 'mytheme_options[stickynavlogo]', array(
    'default'        => 1,
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'stickynavlogo', array(
    'settings' => 'mytheme_options[stickynavlogo]',
    'label'    => __( 'Add the logo to the navigation bar when it is in sticky mode.' ),
    'section'  => 'navbar',
    'type'     => 'checkbox',
) );


$wp_customize->add_setting( 'mytheme_options[stickynavlogoimg]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
    'sanitize_callback' => 'esc_url_raw',
) );

$wp_customize->add_control(
       new WP_Customize_Image_Control(
           $wp_customize,
           'stickynavlogoimg',
           array(
               'label'      => __( 'Select the image for the sticky nav logo if you wish to use a different logo.', 'uikit' ),
               'section'    => 'navbar',
               'settings'   => 'mytheme_options[stickynavlogoimg]',
           )
       )
   );

$wp_customize->add_setting( 'mytheme_options[navbarextra]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'navbarextra', array(
    'settings' => 'mytheme_options[navbarextra]',
    'label'    => __( 'Enter the HTML for any extra navigation bar buttons which will display right aligned.' ),
    'section'  => 'navbar',
    'type'     => 'textarea',
) );

$wp_customize->add_setting( 'mytheme_options[navbarextramob]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'navbarextramob', array(
    'settings' => 'mytheme_options[navbarextramob]',
    'label'    => __( 'Enter the HTML for any extra navigation bar buttons on the mobile navbar.' ),
    'section'  => 'navbar',
    'type'     => 'textarea',
) );

$wp_customize->add_setting( 'mytheme_options[navsearch]', array(
    'default'        => 0,
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'navsearch', array(
    'settings' => 'mytheme_options[navsearch]',
    'label'    => __( 'Add a search to the right side of the navigation bar.' ),
    'section'  => 'navbar',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[gridnav]', array(
    'default'        => 0,
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'gridnav', array(
    'settings' => 'mytheme_options[gridnav]',
    'label'    => __( 'Turn navbar dropdowns into a grid.' ),
    'section'  => 'navbar',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[gridnavtitle]', array(
    'default'        => 0,
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'gridnavtitle', array(
    'settings' => 'mytheme_options[gridnavtitle]',
    'label'    => __( 'Add title to dropdown grid.' ),
    'section'  => 'navbar',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[gridnavimg]', array(
    'default'        => 0,
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'gridnavimg', array(
    'settings' => 'mytheme_options[gridnavimg]',
    'label'    => __( 'Adds area where you can add a background image via css next to the dropdown grid.' ),
    'section'  => 'navbar',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[secondtopnav]', array(
    'default'        => 0,
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'secondtopnav', array(
    'settings' => 'mytheme_options[secondtopnav]',
    'label'    => __( 'Add a second navigation bar to the top of the page.' ),
    'section'  => 'navbar',
    'type'     => 'checkbox',
) );

/* Sidebar Options */

$wp_customize->add_section( 'sidebars', array(
    'title'          => __( 'Sidebars', 'uikit' ),
    'priority'       => 85,
    'description'    => 'Configure sidebar display here.',
) );

$wp_customize->add_setting( 'mytheme_options[sidebarloc]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'sidebarloc', array(
    'settings' => 'mytheme_options[sidebarloc]',
    'label'    => __( 'Sidebar Location' ),
    'section'  => 'sidebars',
    'type'     => 'radio',
    'choices' => array(
				'left' => 'Left',
				'right' => 'Right'),
) );

$wp_customize->add_setting( 'mytheme_options[twosidebars]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'twosidebars', array(
    'settings' => 'mytheme_options[twosidebars]',
    'label'    => __( 'Enable two sidebars, one on eachside.' ),
    'section'  => 'sidebars',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[mobilesidebar]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'mobilesidebar', array(
    'settings' => 'mytheme_options[mobilesidebar]',
    'label'    => __( 'Tick to show sidebars on mobile.' ),
    'section'  => 'sidebars',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[sidebarlast]', array(
    'default'        => 1,
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'sidebarlast', array(
    'settings' => 'mytheme_options[sidebarlast]',
    'label'    => __( 'Show sidebars last on mobile.' ),
    'section'  => 'sidebars',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[centersidebarmob]', array(
    'default'        => 1,
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'centersidebarmob', array(
    'settings' => 'mytheme_options[centersidebarmob]',
    'label'    => __( 'Center Sidebar on Mobile.' ),
    'section'  => 'sidebars',
    'type'     => 'checkbox',
) );

/* Social Media */

$wp_customize->add_section( 'socialmedia', array(
    'title'          => __( 'Social Media', 'uikit' ),
    'priority'       => 85,
    'description'    => 'Configure social media display.',
) );

$wp_customize->add_setting( 'mytheme_options[socialmediabar]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'socialmediabar', array(
    'settings' => 'mytheme_options[socialmediabar]',
    'label'    => __( 'Enable Social Media Bar' ),
    'section'  => 'socialmedia',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[socialmediamob]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'socialmediamob', array(
    'settings' => 'mytheme_options[socialmediamob]',
    'label'    => __( 'Only show Social Media Bar on mobile' ),
    'section'  => 'socialmedia',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[stickysocial]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'stickysocial', array(
    'settings' => 'mytheme_options[stickysocial]',
    'label'    => __( 'Sticky Social Media Bar' ),
    'section'  => 'socialmedia',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[smbarcol]', array(
    'default'        => '#444444',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control(
	new WP_Customize_Color_Control(
	$wp_customize,
	'smbarcol',
	array(
		'label'      => __( 'Social Media Bar Background Colour', 'uikit' ),
		'section'    => 'socialmedia',
		'settings'   => 'mytheme_options[smbarcol]',
	) )
);

$wp_customize->add_setting( 'mytheme_options[socialmediafb]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
    'sanitize_callback' => 'esc_url_raw',
) );

$wp_customize->add_control( 'socialmediafb', array(
    'settings' => 'mytheme_options[socialmediafb]',
    'label'    => __( 'Enter your Facebook url here to display button. Leave blank to hide.' ),
    'section'  => 'socialmedia',
    'type'     => 'text',
) );

$wp_customize->add_setting( 'mytheme_options[socialmediatwit]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
    'sanitize_callback' => 'esc_url_raw',
) );

$wp_customize->add_control( 'socialmediatwit', array(
    'settings' => 'mytheme_options[socialmediatwit]',
    'label'    => __( 'Enter your Twitter url here to display button. Leave blank to hide.' ),
    'section'  => 'socialmedia',
    'type'     => 'text',
) );

$wp_customize->add_setting( 'mytheme_options[socialmediainst]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
    'sanitize_callback' => 'esc_url_raw',
) );

$wp_customize->add_control( 'socialmediainst', array(
    'settings' => 'mytheme_options[socialmediainst]',
    'label'    => __( 'Enter your Instagram url here to display button. Leave blank to hide.' ),
    'section'  => 'socialmedia',
    'type'     => 'text',
) );

$wp_customize->add_setting( 'mytheme_options[socialmediali]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
    'sanitize_callback' => 'esc_url_raw',
) );

$wp_customize->add_control( 'socialmediali', array(
    'settings' => 'mytheme_options[socialmediali]',
    'label'    => __( 'Enter your LinkedIn url here to display button. Leave blank to hide.' ),
    'section'  => 'socialmedia',
    'type'     => 'text',
) );

$wp_customize->add_setting( 'mytheme_options[socialmediaphone]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'socialmediaphone', array(
    'settings' => 'mytheme_options[socialmediaphone]',
    'label'    => __( 'Enter your phone number here to display button. Leave blank to hide.' ),
    'section'  => 'socialmedia',
    'type'     => 'text',
) );

$wp_customize->add_setting( 'mytheme_options[socialmediaemail]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
    'sanitize_callback' => 'is_email',
) );

$wp_customize->add_control( 'socialmediaemail', array(
    'settings' => 'mytheme_options[socialmediaemail]',
    'label'    => __( 'Enter your Email address here to display button. Leave blank to hide.' ),
    'section'  => 'socialmedia',
    'type'     => 'text',
) );

$wp_customize->add_setting( 'mytheme_options[socialmediabuttons]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'socialmediabuttons', array(
    'settings' => 'mytheme_options[socialmediabuttons]',
    'label'    => __( 'Extra Social Media Bar buttons HTML code' ),
    'section'  => 'socialmedia',
    'type'     => 'textarea',
) );

/* Header Options */

$wp_customize->add_section( 'headers', array(
    'title'          => __( 'Header', 'uikit' ),
    'priority'       => 30,
    'description'    => 'Change header settings here.',
) );

$wp_customize->add_setting( 'mytheme_options[header]', array(
    'default'        => 1,
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'header', array(
    'settings' => 'mytheme_options[header]',
    'label'    => __( 'Display the header above navigation.' ),
    'section'  => 'headers',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[headermob]', array(
    'default'        => 1,
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'headermob', array(
    'settings' => 'mytheme_options[headermob]',
    'label'    => __( 'Display the header on mobile.' ),
    'section'  => 'headers',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[headerslider]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'headerslider', array(
    'settings' => 'mytheme_options[headerslider]',
    'label'    => __( 'Display a slider in the header, customise in the header slider options.' ),
    'section'  => 'headers',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[headerheight]', array(
    'default'        => '140px',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'headerheight', array(
    'settings' => 'mytheme_options[headerheight]',
    'label'    => __( 'Enter the height you want the header to be in pixels. Only relevant if there is no header image.' ),
    'section'  => 'headers',
    'type'     => 'text',
) );

$wp_customize->add_setting( 'mytheme_options[headercolour]', array(
    'default'        => '#FFFFFF',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control(
	new WP_Customize_Color_Control(
	$wp_customize,
	'headercolour',
	array(
		'label'      => __( 'Header Background Colour', 'uikit' ),
		'section'    => 'headers',
		'settings'   => 'mytheme_options[headercolour]',
	) )
);

$wp_customize->add_setting( 'mytheme_options[headertextcolour]', array(
    'default'        => '#444444',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control(
	new WP_Customize_Color_Control(
	$wp_customize,
	'headertextcolour',
	array(
		'label'      => __( 'Header Text Colour', 'uikit' ),
		'section'    => 'headers',
		'settings'   => 'mytheme_options[headertextcolour]',
	) )
);

/* Header Slider Options */

$wp_customize->add_section( 'headersliders', array(
    'title'          => __( 'Header Slider', 'uikit' ),
    'priority'       => 30,
    'description'    => 'Change header slider settings here, to enable a slider in the header, tick the corresponding option in the Header section.',
) );

$wp_customize->add_setting( 'mytheme_options[hoverlaydisp]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'hoverlaydisp', array(
    'settings' => 'mytheme_options[hoverlaydisp]',
    'label'    => __( 'When should the overlay be displayed?' ),
    'section'  => 'headersliders',
    'type'     => 'select',
		  'choices' => array(
		    '' => 'Always',
		    'uk-overlay-active' => 'Slide Active',
		    'uk-overlay-hover' => 'On Hover',
		  )
) );

$wp_customize->add_setting( 'mytheme_options[hremovepadding]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'hremovepadding', array(
    'settings' => 'mytheme_options[hremovepadding]',
    'label'    => __( 'Remove overlay padding.' ),
    'section'  => 'headersliders',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[holbg]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'holbg', array(
    'settings' => 'mytheme_options[holbg]',
    'label'    => __( 'Display overlay background' ),
    'section'  => 'headersliders',
    'type'     => 'select',
		  'choices' => array(
		   'uk-overlay-background' => 'Yes',
		   '' => 'No',
		  )
) );

$wp_customize->add_setting( 'mytheme_options[hlogoloc]', array(
    'default'        => 'uk-flex-middle',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'hlogoloc', array(
    'settings' => 'mytheme_options[hlogoloc]',
    'label'    => __( 'Logo/Title Location' ),
    'section'  => 'headersliders',
    'type'     => 'select',
    'choices' => array(
		   'uk-flex-top' => 'Top Left',
		   'uk-flex-middle' => 'Middle Left',
		   'uk-flex-bottom' => 'Bottom Left',
		   'uk-flex-top uk-flex-center' => 'Top Center',
		   'uk-flex-middle uk-flex-center' => 'Middle Center',
		   'uk-flex-bottom uk-flex-center' => 'Bottom Center',
	 	   'uk-flex-top uk-flex-right' => 'Top Right',
		   'uk-flex-middle uk-flex-right' => 'Middle Right',
		   'uk-flex-bottom uk-flex-right' => 'Bottom Right',

		  )
) );

$wp_customize->add_setting( 'mytheme_options[htextaligntitle]', array(
    'default'        => 'uk-text-center',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'htextaligntitle', array(
    'settings' => 'mytheme_options[htextaligntitle]',
    'label'    => __( 'Site Title Text Align' ),
    'section'  => 'headersliders',
    'type'     => 'select',
		  'choices' => array(
		    '' => 'Left',
    		'uk-text-center' => 'Center',
    		'uk-text-right' => 'Right',
		  )
) );

$wp_customize->add_setting( 'mytheme_options[hshowdes]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'hshowdes', array(
    'settings' => 'mytheme_options[hshowdes]',
    'label'    => __( 'Show Description' ),
    'section'  => 'headersliders',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[hcustomdes]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'hcustomdes', array(
    'settings' => 'mytheme_options[hcustomdes]',
    'label'    => __( 'Enter a Custom Description if required. Otherwise leave blank and it will use the site description you entered in WordPress.' ),
    'section'  => 'headersliders',
    'type'     => 'textarea',
) );

$wp_customize->add_setting( 'mytheme_options[hdesloc]', array(
    'default'        => 'uk-flex-middle uk-flex-center',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'hdesloc', array(
    'settings' => 'mytheme_options[hdesloc]',
    'label'    => __( 'Site Description Location' ),
    'section'  => 'headersliders',
    'type'     => 'select',
  		  'choices' => array(
    		'uk-flex-top' => 'Top Left',
    		'uk-flex-middle' => 'Middle Left',
    		'uk-flex-bottom' => 'Bottom Left',
    		'uk-flex-top uk-flex-center' => 'Top Center',
    		'uk-flex-middle uk-flex-center' => 'Middle Center',
    		'uk-flex-bottom uk-flex-center' => 'Bottom Center',
    		'uk-flex-top uk-flex-right' => 'Top Right',
    		'uk-flex-middle uk-flex-right' => 'Middle Right',
    		'uk-flex-bottom uk-flex-right' => 'Bottom Right',

		 )
) );

$wp_customize->add_setting( 'mytheme_options[htextaligndes]', array(
    'default'        => 'uk-text-center',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'htextaligndes', array(
    'settings' => 'mytheme_options[htextaligndes]',
    'label'    => __( 'Site Description Text Align' ),
    'section'  => 'headersliders',
    'type'     => 'select',
  		  'choices' => array(
    		'' => 'Left',
    		'uk-text-center' => 'Center',
    		'uk-text-right' => 'Right',
		 )
) );

$wp_customize->add_setting( 'mytheme_options[hoverlayanim]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'hoverlayanim', array(
    'settings' => 'mytheme_options[hoverlayanim]',
    'label'    => __( 'Overlay Animation' ),
    'section'  => 'headersliders',
    'type'     => 'select',
    'choices' => array(
    		'' => 'None',
    		'uk-overlay-slide-top' => 'Slide Top',
    		'uk-overlay-slide-bottom' => 'Slide Bottom',
    		'uk-overlay-slide-left' => 'Slide Left',
    		'uk-overlay-slide-right' => 'Slide Right',
    		'uk-overlay-fade' => 'Fade',
    		'uk-overlay-scale' => 'Scale',
    		'uk-overlay-spin' => 'Spin',

		 )
) );

$wp_customize->add_setting( 'mytheme_options[hslidermobdis]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'hslidermobdis', array(
    'settings' => 'mytheme_options[hslidermobdis]',
    'label'    => __( 'Disable the slider on mobile even if the header is enabled' ),
    'section'  => 'headersliders',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[harrownav]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'harrownav', array(
    'settings' => 'mytheme_options[harrownav]',
    'label'    => __( 'Enable arrow navigation on the slider to switch betwene slides' ),
    'section'  => 'headersliders',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[hkenburns]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'hkenburns', array(
    'settings' => 'mytheme_options[hkenburns]',
    'label'    => __( 'Kenburns Animation Effect' ),
    'section'  => 'headersliders',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[hautoplay]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'hautoplay', array(
    'settings' => 'mytheme_options[hautoplay]',
    'label'    => __( 'Autoplay' ),
    'section'  => 'headersliders',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[hslidernav]', array(
    'default'        => 'none',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'hslidernav', array(
    'settings' => 'mytheme_options[hslidernav]',
    'label'    => __( 'Slideshow navigation' ),
    'section'  => 'headersliders',
    'type'     => 'select',
		  'choices' => array(
		    'none' => 'None',
		    'dotnav' => 'Dotnav',
		  )
) );

$wp_customize->add_setting( 'mytheme_options[hanim]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'hanim', array(
    'settings' => 'mytheme_options[hanim]',
    'label'    => __( 'Transition Animation' ),
    'section'  => 'headersliders',
    'type'     => 'select',
		  'choices' => array(
		    '' => 'None',
		    'animation: \'random-fx\'' => 'Random',
		    'animation: \'slice-down\'' => 'Slice Down',
		    'animation: \'slice-up\'' => 'Slice Up',
			'animation: \'slice-up-down\'' => 'Slice Up Down',
		    'animation: \'fade\'' => 'Fade',
		    'animation: \'scale\'' => 'Scale',
		    'animation: \'scroll\'' => 'Scroll',
    		'animation: \'swipe\'' => 'Swipe',
    		'animation: \'fold\'' => 'Fold',
    		'animation: \'puzzle\'' => 'Puzzle',
    		'animation: \'boxes\'' => 'Boxes',
    		'animation: \'boxes-reverse\'' => 'Boxes Reverse',

		  )
) );

$wp_customize->add_setting( 'mytheme_options[hslideone]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
    'sanitize_callback' => 'esc_url_raw',
) );

$wp_customize->add_control(
       new WP_Customize_Image_Control(
           $wp_customize,
           'hslideone',
           array(
               'label'      => __( 'Slider Image One', 'uikit' ),
               'section'    => 'headersliders',
               'settings'   => 'mytheme_options[hslideone]',
           )
       )
   );

$wp_customize->add_setting( 'mytheme_options[hslidetwo]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
    'sanitize_callback' => 'esc_url_raw',
) );

$wp_customize->add_control(
       new WP_Customize_Image_Control(
           $wp_customize,
           'hslidetwo',
           array(
               'label'      => __( 'Slider Image Two', 'uikit' ),
               'section'    => 'headersliders',
               'settings'   => 'mytheme_options[hslidetwo]',
           )
       )
   );

$wp_customize->add_setting( 'mytheme_options[hslidethr]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
    'sanitize_callback' => 'esc_url_raw',
) );

$wp_customize->add_control(
       new WP_Customize_Image_Control(
           $wp_customize,
           'hslidethr',
           array(
               'label'      => __( 'Slider Image Three', 'uikit' ),
               'section'    => 'headersliders',
               'settings'   => 'mytheme_options[hslidethr]',
           )
       )
   );

$wp_customize->add_setting( 'mytheme_options[hslidefr]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
    'sanitize_callback' => 'esc_url_raw',
) );

$wp_customize->add_control(
       new WP_Customize_Image_Control(
           $wp_customize,
           'hslidefr',
           array(
               'label'      => __( 'Slider Image Four', 'uikit' ),
               'section'    => 'headersliders',
               'settings'   => 'mytheme_options[hslidefr]',
           )
       )
   );

/* Front Page */

$wp_customize->add_section( 'front', array(
    'title'          => __( 'Front Page', 'uikit' ),
    'priority'       => 80,
    'description'    => 'Change front page options here, including the front page slider.',
) );

$wp_customize->add_setting( 'mytheme_options[frontsidebar]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'frontsidebar', array(
    'settings' => 'mytheme_options[frontsidebar]',
    'label'    => __( 'Sidebars on Front Page' ),
    'section'  => 'front',
    'type'     => 'checkbox',
) );


$wp_customize->add_setting( 'mytheme_options[slider]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'slider', array(
    'settings' => 'mytheme_options[slider]',
    'label'    => __( 'Front Page Slider' ),
    'section'  => 'front',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[sliderallpages]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'sliderallpages', array(
    'settings' => 'mytheme_options[sliderallpages]',
    'label'    => __( 'Front Page Slider on All Pages' ),
    'section'  => 'front',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[sliderfullwidth]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'sliderfullwidth', array(
    'settings' => 'mytheme_options[sliderfullwidth]',
    'label'    => __( 'Do you want the slider to fill the full width of the browser regardless of the content width option? This is only applicable when Fullscreen and Content Width is checked' ),
    'section'  => 'front',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[arrownav]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'arrownav', array(
    'settings' => 'mytheme_options[arrownav]',
    'label'    => __( 'Arrow Navigation on Slider' ),
    'section'  => 'front',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[kenburns]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'kenburns', array(
    'settings' => 'mytheme_options[kenburns]',
    'label'    => __( 'Kenburns Animation Effect' ),
    'section'  => 'front',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[autoplay]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'autoplay', array(
    'settings' => 'mytheme_options[autoplay]',
    'label'    => __( 'Autoplay' ),
    'section'  => 'front',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[slidernav]', array(
    'default'        => 'none',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'slidernav', array(
    'settings' => 'mytheme_options[slidernav]',
    'label'    => __( 'Slideshow navigation' ),
    'section'  => 'front',
    'type'     => 'select',
		  'choices' => array(
		    'none' => 'None',
		    'dotnav' => 'Dotnav',
		  )
) );

$wp_customize->add_setting( 'mytheme_options[anim]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'anim', array(
    'settings' => 'mytheme_options[anim]',
    'label'    => __( 'Transition Animation' ),
    'section'  => 'front',
    'type'     => 'select',
		  'choices' => array(
		    '' => 'None',
		    'animation: \'random-fx\'' => 'Random',
		    'animation: \'slice-down\'' => 'Slice Down',
		    'animation: \'slice-up\'' => 'Slice Up',
			'animation: \'slice-up-down\'' => 'Slice Up Down',
		    'animation: \'fade\'' => 'Fade',
		    'animation: \'scale\'' => 'Scale',
		    'animation: \'scroll\'' => 'Scroll',
    		'animation: \'swipe\'' => 'Swipe',
    		'animation: \'fold\'' => 'Fold',
    		'animation: \'puzzle\'' => 'Puzzle',
    		'animation: \'boxes\'' => 'Boxes',
    		'animation: \'boxes-reverse\'' => 'Boxes Reverse',

		  )
) );

$wp_customize->add_setting( 'mytheme_options[overlaydisp]', array(
    'default'        => 'uk-overlay-active',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'overlaydisp', array(
    'settings' => 'mytheme_options[overlaydisp]',
    'label'    => __( 'Overlay Display' ),
    'section'  => 'front',
    'type'     => 'select',
			'choices' => array(
				'' => 'Always',
				'uk-overlay-active' => 'Slide Active',
				'uk-overlay-hover' => 'On Hover',
			)
) );

$wp_customize->add_setting( 'mytheme_options[slideone]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
    'sanitize_callback' => 'esc_url_raw',
) );

$wp_customize->add_control(
       new WP_Customize_Image_Control(
           $wp_customize,
           'slideone',
           array(
               'label'      => __( 'Slider Image One', 'uikit' ),
               'section'    => 'front',
               'settings'   => 'mytheme_options[slideone]',
           )
       )
   );

$wp_customize->add_setting( 'mytheme_options[overlayone]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'overlayone', array(
    'settings' => 'mytheme_options[overlayone]',
    'label'    => __( 'Display overlay on Slide One' ),
    'section'  => 'front',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[overlaydesone]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'overlaydesone', array(
    'settings' => 'mytheme_options[overlaydesone]',
    'label'    => __( 'Overlay Text for Slide One. HTML can be used.' ),
    'section'  => 'front',
    'type'     => 'textarea',
) );

$wp_customize->add_setting( 'mytheme_options[olbgone]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'olbgone', array(
    'settings' => 'mytheme_options[olbgone]',
    'label'    => __( 'Overlay background on Slide One' ),
    'section'  => 'front',
    'type'     => 'select',
			'choices' => array(
				'uk-overlay-background' => 'Yes',
				'' => 'No',
			)
) );

$wp_customize->add_setting( 'mytheme_options[overlaylocone]', array(
    'default'        => 'uk-flex-middle uk-flex-center',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'overlaylocone', array(
    'settings' => 'mytheme_options[overlaylocone]',
    'label'    => __( 'Overlay Location Slide One' ),
    'section'  => 'front',
    'type'     => 'select',
			'choices' => array(
				'uk-flex-top' => 'Top Left',
				'uk-flex-middle' => 'Middle Left',
				'uk-flex-bottom' => 'Bottom Left',
				'uk-flex-top uk-flex-center' => 'Top Center',
				'uk-flex-middle uk-flex-center' => 'Middle Center',
				'uk-flex-bottom uk-flex-center' => 'Bottom Center',
				'uk-flex-top uk-flex-right' => 'Top Right',
				'uk-flex-middle uk-flex-right' => 'Middle Right',
				'uk-flex-bottom uk-flex-right' => 'Bottom Right',

			)
) );

$wp_customize->add_setting( 'mytheme_options[textalignone]', array(
    'default'        => 'uk-text-center',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'textalignone', array(
    'settings' => 'mytheme_options[textalignone]',
    'label'    => __( 'Text Align Slide One' ),
    'section'  => 'front',
    'type'     => 'select',
			'choices' => array(
				'' => 'Left',
				'uk-text-center' => 'Center',
				'uk-text-right' => 'Right',
			)
) );

$wp_customize->add_setting( 'mytheme_options[overlayanimone]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'overlayanimone', array(
    'settings' => 'mytheme_options[overlayanimone]',
    'label'    => __( 'Overlay Animation Slide One' ),
    'section'  => 'front',
    'type'     => 'select',
			'choices' => array(
				'' => 'None',
				'uk-overlay-slide-top' => 'Slide Top',
				'uk-overlay-slide-bottom' => 'Slide Bottom',
				'uk-overlay-slide-left' => 'Slide Left',
				'uk-overlay-slide-right' => 'Slide Right',
				'uk-overlay-fade' => 'Fade',
				'uk-overlay-scale' => 'Scale',
				'uk-overlay-spin' => 'Spin',

			)
) );

$wp_customize->add_setting( 'mytheme_options[slidetwo]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
    'sanitize_callback' => 'esc_url_raw',
) );

$wp_customize->add_control(
       new WP_Customize_Image_Control(
           $wp_customize,
           'slidetwo',
           array(
               'label'      => __( 'Slider Image Two', 'uikit' ),
               'section'    => 'front',
               'settings'   => 'mytheme_options[slidetwo]',
           )
       )
   );

$wp_customize->add_setting( 'mytheme_options[overlaytwo]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'overlaytwo', array(
    'settings' => 'mytheme_options[overlaytwo]',
    'label'    => __( 'Display overlay on Slide Two' ),
    'section'  => 'front',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[overlaydestwo]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'overlaydestwo', array(
    'settings' => 'mytheme_options[overlaydestwo]',
    'label'    => __( 'Overlay Text for Slide Two. HTML can be used.' ),
    'section'  => 'front',
    'type'     => 'textarea',
) );

$wp_customize->add_setting( 'mytheme_options[olbgtwo]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'olbgtwo', array(
    'settings' => 'mytheme_options[olbgtwo]',
    'label'    => __( 'Overlay background on Slide Two' ),
    'section'  => 'front',
    'type'     => 'select',
			'choices' => array(
				'uk-overlay-background' => 'Yes',
				'' => 'No',
			)
) );

$wp_customize->add_setting( 'mytheme_options[overlayloctwo]', array(
    'default'        => 'uk-flex-middle uk-flex-center',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'overlayloctwo', array(
    'settings' => 'mytheme_options[overlayloctwo]',
    'label'    => __( 'Overlay Location Slide Two' ),
    'section'  => 'front',
    'type'     => 'select',
			'choices' => array(
				'uk-flex-top' => 'Top Left',
				'uk-flex-middle' => 'Middle Left',
				'uk-flex-bottom' => 'Bottom Left',
				'uk-flex-top uk-flex-center' => 'Top Center',
				'uk-flex-middle uk-flex-center' => 'Middle Center',
				'uk-flex-bottom uk-flex-center' => 'Bottom Center',
				'uk-flex-top uk-flex-right' => 'Top Right',
				'uk-flex-middle uk-flex-right' => 'Middle Right',
				'uk-flex-bottom uk-flex-right' => 'Bottom Right',

			)
) );

$wp_customize->add_setting( 'mytheme_options[textaligntwo]', array(
    'default'        => 'uk-text-center',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'textaligntwo', array(
    'settings' => 'mytheme_options[textaligntwo]',
    'label'    => __( 'Text Align Slide Two' ),
    'section'  => 'front',
    'type'     => 'select',
			'choices' => array(
				'' => 'Left',
				'uk-text-center' => 'Center',
				'uk-text-right' => 'Right',
			)
) );

$wp_customize->add_setting( 'mytheme_options[overlayanimtwo]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'overlayanimtwo', array(
    'settings' => 'mytheme_options[overlayanimtwo]',
    'label'    => __( 'Overlay Animation Slide Two' ),
    'section'  => 'front',
    'type'     => 'select',
			'choices' => array(
				'' => 'None',
				'uk-overlay-slide-top' => 'Slide Top',
				'uk-overlay-slide-bottom' => 'Slide Bottom',
				'uk-overlay-slide-left' => 'Slide Left',
				'uk-overlay-slide-right' => 'Slide Right',
				'uk-overlay-fade' => 'Fade',
				'uk-overlay-scale' => 'Scale',
				'uk-overlay-spin' => 'Spin',

			)
) );

$wp_customize->add_setting( 'mytheme_options[slidethr]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
    'sanitize_callback' => 'esc_url_raw',
) );

$wp_customize->add_control(
       new WP_Customize_Image_Control(
           $wp_customize,
           'slidethr',
           array(
               'label'      => __( 'Slider Image Three', 'uikit' ),
               'section'    => 'front',
               'settings'   => 'mytheme_options[slidethr]',
           )
       )
   );

$wp_customize->add_setting( 'mytheme_options[overlaythr]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'overlaythr', array(
    'settings' => 'mytheme_options[overlaythr]',
    'label'    => __( 'Display overlay on Slide Three' ),
    'section'  => 'front',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[overlaydesthr]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'overlaydesthr', array(
    'settings' => 'mytheme_options[overlaydesthr]',
    'label'    => __( 'Overlay Text for Slide Three. HTML can be used.' ),
    'section'  => 'front',
    'type'     => 'textarea',
) );

$wp_customize->add_setting( 'mytheme_options[olbgthr]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'olbgthr', array(
    'settings' => 'mytheme_options[olbgthr]',
    'label'    => __( 'Overlay background on Slide Three' ),
    'section'  => 'front',
    'type'     => 'select',
			'choices' => array(
				'uk-overlay-background' => 'Yes',
				'' => 'No',
			)
) );

$wp_customize->add_setting( 'mytheme_options[overlaylocthr]', array(
    'default'        => 'uk-flex-middle uk-flex-center',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'overlaylocthr', array(
    'settings' => 'mytheme_options[overlaylocthr]',
    'label'    => __( 'Overlay Location Slide Three' ),
    'section'  => 'front',
    'type'     => 'select',
			'choices' => array(
				'uk-flex-top' => 'Top Left',
				'uk-flex-middle' => 'Middle Left',
				'uk-flex-bottom' => 'Bottom Left',
				'uk-flex-top uk-flex-center' => 'Top Center',
				'uk-flex-middle uk-flex-center' => 'Middle Center',
				'uk-flex-bottom uk-flex-center' => 'Bottom Center',
				'uk-flex-top uk-flex-right' => 'Top Right',
				'uk-flex-middle uk-flex-right' => 'Middle Right',
				'uk-flex-bottom uk-flex-right' => 'Bottom Right',

			)
) );

$wp_customize->add_setting( 'mytheme_options[textalignthr]', array(
    'default'        => 'uk-text-center',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'textalignthr', array(
    'settings' => 'mytheme_options[textalignthr]',
    'label'    => __( 'Text Align Slide Three' ),
    'section'  => 'front',
    'type'     => 'select',
			'choices' => array(
				'' => 'Left',
				'uk-text-center' => 'Center',
				'uk-text-right' => 'Right',
			)
) );

$wp_customize->add_setting( 'mytheme_options[overlayanimthr]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'overlayanimthr', array(
    'settings' => 'mytheme_options[overlayanimthr]',
    'label'    => __( 'Overlay Animation Slide Three' ),
    'section'  => 'front',
    'type'     => 'select',
			'choices' => array(
				'' => 'None',
				'uk-overlay-slide-top' => 'Slide Top',
				'uk-overlay-slide-bottom' => 'Slide Bottom',
				'uk-overlay-slide-left' => 'Slide Left',
				'uk-overlay-slide-right' => 'Slide Right',
				'uk-overlay-fade' => 'Fade',
				'uk-overlay-scale' => 'Scale',
				'uk-overlay-spin' => 'Spin',

			)
) );

$wp_customize->add_setting( 'mytheme_options[slidefr]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
    'sanitize_callback' => 'esc_url_raw',
) );

$wp_customize->add_control(
       new WP_Customize_Image_Control(
           $wp_customize,
           'slidefr',
           array(
               'label'      => __( 'Slider Image Four', 'uikit' ),
               'section'    => 'front',
               'settings'   => 'mytheme_options[slidefr]',
           )
       )
   );

$wp_customize->add_setting( 'mytheme_options[overlayfr]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'overlayfr', array(
    'settings' => 'mytheme_options[overlayfr]',
    'label'    => __( 'Display overlay on Slide Four' ),
    'section'  => 'front',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[overlaydesfr]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'overlaydesfr', array(
    'settings' => 'mytheme_options[overlaydesfr]',
    'label'    => __( 'Overlay Text for Slide Four. HTML can be used.' ),
    'section'  => 'front',
    'type'     => 'textarea',
) );

$wp_customize->add_setting( 'mytheme_options[olbgfr]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'olbgfr', array(
    'settings' => 'mytheme_options[olbgfr]',
    'label'    => __( 'Overlay background on Slide Four' ),
    'section'  => 'front',
    'type'     => 'select',
			'choices' => array(
				'uk-overlay-background' => 'Yes',
				'' => 'No',
			)
) );

$wp_customize->add_setting( 'mytheme_options[overlaylocfr]', array(
    'default'        => 'uk-flex-middle uk-flex-center',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'overlaylocfr', array(
    'settings' => 'mytheme_options[overlaylocfr]',
    'label'    => __( 'Overlay Location Slide Four' ),
    'section'  => 'front',
    'type'     => 'select',
			'choices' => array(
				'uk-flex-top' => 'Top Left',
				'uk-flex-middle' => 'Middle Left',
				'uk-flex-bottom' => 'Bottom Left',
				'uk-flex-top uk-flex-center' => 'Top Center',
				'uk-flex-middle uk-flex-center' => 'Middle Center',
				'uk-flex-bottom uk-flex-center' => 'Bottom Center',
				'uk-flex-top uk-flex-right' => 'Top Right',
				'uk-flex-middle uk-flex-right' => 'Middle Right',
				'uk-flex-bottom uk-flex-right' => 'Bottom Right',

			)
) );

$wp_customize->add_setting( 'mytheme_options[textalignfr]', array(
    'default'        => 'uk-text-center',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'textalignfr', array(
    'settings' => 'mytheme_options[textalignfr]',
    'label'    => __( 'Text Align Slide Four' ),
    'section'  => 'front',
    'type'     => 'select',
			'choices' => array(
				'' => 'Left',
				'uk-text-center' => 'Center',
				'uk-text-right' => 'Right',
			)
) );

$wp_customize->add_setting( 'mytheme_options[overlayanimfr]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'overlayanimfr', array(
    'settings' => 'mytheme_options[overlayanimfr]',
    'label'    => __( 'Overlay Animation Slide Four' ),
    'section'  => 'front',
    'type'     => 'select',
			'choices' => array(
				'' => 'None',
				'uk-overlay-slide-top' => 'Slide Top',
				'uk-overlay-slide-bottom' => 'Slide Bottom',
				'uk-overlay-slide-left' => 'Slide Left',
				'uk-overlay-slide-right' => 'Slide Right',
				'uk-overlay-fade' => 'Fade',
				'uk-overlay-scale' => 'Scale',
				'uk-overlay-spin' => 'Spin',

			)
) );

/* Woocommerce Options */

if (class_exists('woocommerce')){

    $wp_customize->add_section( 'woocommerce', array(
    'title'          => __( 'Woocommerce', 'uikit' ),
    'priority'       => 85,
    'description'    => 'Change any woocommerce options here.',
) );

$wp_customize->add_setting( 'mytheme_options[wooshopsidebar]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'wooshopsidebar', array(
    'settings' => 'mytheme_options[wooshopsidebar]',
    'label'    => __( 'Disable the sidebar which displays on the shop' ),
    'section'  => 'woocommerce',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[wooproductsidebar]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'wooproductsidebar', array(
    'settings' => 'mytheme_options[wooproductsidebar]',
    'label'    => __( 'Disable the sidebar which displays on the product page' ),
    'section'  => 'woocommerce',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[woodoublesidebars]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'woodoublesidebars', array(
    'settings' => 'mytheme_options[woodoublesidebars]',
    'label'    => __( 'Enable two sidebars on the shop and product woocommerce pages.' ),
    'section'  => 'woocommerce',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[wooturnoff]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'wooturnoff', array(
    'settings' => 'mytheme_options[wooturnoff]',
    'label'    => __( 'Turn off shop - disables the cart and removes any add to cart buttons.' ),
    'section'  => 'woocommerce',
    'type'     => 'checkbox',
) );

$wp_customize->add_setting( 'mytheme_options[woowarningmsg]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'woowarningmsg', array(
    'settings' => 'mytheme_options[woowarningmsg]',
    'label'    => __( 'Displays a Warning Message at the top of your site with the following text' ),
    'section'  => 'woocommerce',
    'type'     => 'text',
) );


}

/* Footer Options */

    $wp_customize->add_section( 'footer', array(
    'title'          => __( 'Footer', 'uikit' ),
    'priority'       => 85,
    'description'    => 'Change any footer options here.',
) );

$wp_customize->add_setting( 'mytheme_options[footerbg]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'footerbg', array(
    'settings' => 'mytheme_options[footerbg]',
    'label'    => __( 'Footer Background Colour' ),
    'section'  => 'footer',
    'type'     => 'text',
) );

$wp_customize->add_setting( 'mytheme_options[footertextcol]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control(
	new WP_Customize_Color_Control(
	$wp_customize,
	'footertextcol',
	array(
		'label'      => __( 'Footer Text Colour', 'uikit' ),
		'section'    => 'footer',
		'settings'   => 'mytheme_options[footertextcol]',
	) )
);

$wp_customize->add_setting( 'mytheme_options[footertext]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'footertext', array(
    'settings' => 'mytheme_options[footertext]',
    'label'    => __( 'Enter footer text here. HTML can be used.' ),
    'section'  => 'footer',
    'type'     => 'textarea',
) );

/* Custom CSS */

    $wp_customize->add_section( 'customcss', array(
    'title'          => __( 'Custom CSS', 'uikit' ),
    'priority'       => 200,
    'description'    => 'Enter any custom CSS here.',
) );

$wp_customize->add_setting( 'mytheme_options[custom_css]', array(
    'default'        => '',
    'type'           => 'option',
    'capability'     => 'edit_theme_options',
) );

$wp_customize->add_control( 'custom_css', array(
    'settings' => 'mytheme_options[custom_css]',
    'label'    => __( 'Custom CSS' ),
    'section'  => 'customcss',
    'type'     => 'textarea',
) );













}

?>
