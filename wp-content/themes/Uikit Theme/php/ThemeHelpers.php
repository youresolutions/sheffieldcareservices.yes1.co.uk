<?php
/**
 * Provides some helpers to use within the views.
 */
class ThemeHelpers
{
    /**
     * Display an optional post thumbnail.
     *
     * Wraps the post thumbnail in an anchor element on index
     * views, or a div element when on single views.
     */
     public function postThumbnail()
    {
        if (post_password_required() || is_attachment() || !has_post_thumbnail()) {
            return;
        }
        if (is_singular()) {
            $title = get_the_title();
            $title = preg_replace('/\s+/', '', $title);
            if (wp_is_mobile()) {
             $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'thumbnail');
            $fullurl = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'full');
            $image = get_the_post_thumbnail();
            $moreattrs = 'id="postimg" data-zoom-image="'.$fullurl.'"';
            $image = str_replace('class=', $moreattrs.' class=', $image );
            echo $image;
            }
            else {
            $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'thumbnail');
            $fullurl = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'full');
            $image = get_the_post_thumbnail();
             echo '<a class="uk-thumbnail" href="';
            echo $fullurl;
            echo '" data-uk-lightbox="{group:\'';
            echo $title;
            echo '\'}">';
            $moreattrs = 'id="postimg" data-zoom-image="'.$fullurl.'"';
            $image = str_replace('class=', $moreattrs.' class=', $image );
            echo $image;
            echo '</a>'; }
        } else {
            echo '<a class="uk-thumbnail" href="';
            the_permalink();
            echo '">';
            the_post_thumbnail();
            echo '</a>';
        }
    }
    /**
     * Displays the pagination for the posts overview page (and search and archive)
     */
    public function getPostsPagination()
    {
        $pagination = paginate_links(array('type' => 'array', 'mid_size' => 3, 'prev_next' => false));
        if ($pagination == null) return;
        $returner = array();
        $returner[] = '<ul class="uk-pagination">';
        $returner[] = '<li class="uk-pagination-previous">' . get_previous_posts_link() . '</li>';
        for ($i = 0; $i < sizeof($pagination); $i++) {
            $returner[] = '<li class="uk-hidden-small">' . $pagination[$i] . '</li>';
        }
        $returner[] = '<li class="uk-pagination-next">' . get_next_posts_link() . '</li>';
        $returner[] = '</ul>';
        return implode('', $returner);
    }
}
