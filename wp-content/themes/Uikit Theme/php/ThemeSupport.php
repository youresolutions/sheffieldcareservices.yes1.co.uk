<?php

/**
 * Manages all theme-support-related modifications.
 */
class ThemeSupport
{

    public function __construct()
    {
        // add HTML5 support
        add_theme_support('html5', array('search-form', 'comment-form', 'comment-list', 'gallery', 'caption'));

        // add Featured Image support
        add_theme_support('post-thumbnails');

        // add a customizable header (with image and color)
        add_theme_support('custom-header', array('height' => 400, 'width' => 2500, 'default-text-color' => '444',));

        if (class_exists('woocommerce')){
        // add Woocommerce Gallery support
        add_theme_support( 'wc-product-gallery-zoom' );
        add_theme_support( 'wc-product-gallery-lightbox' );
        add_theme_support( 'wc-product-gallery-slider' );
        }
    }

}
