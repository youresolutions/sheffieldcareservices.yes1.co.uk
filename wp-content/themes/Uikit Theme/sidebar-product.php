<?php
/**
 * Default template for sidebar
 */
?>

<?php if (is_active_sidebar('sidebar-product')) : ?>
    <div id="product-sidebar" class="widget-area" role="complementary" style="padding:10px;">        
        <?php dynamic_sidebar('sidebar-product'); ?>
    </div>
<?php endif; ?>
