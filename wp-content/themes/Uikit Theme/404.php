<?php
/**
 * 404 template
 */

get_header();
get_template_part('elements/base/header');
get_template_part('elements/base/navigation');
get_template_part('elements/base/precontent'); ?>

    <section class="uk-height-1-1 uk-vertical-align uk-text-center">
        <div class="uk-vertical-align-middle">
            <h1>404</h1>
            <p><?php echo __('Sorry, but we could not find, what you were looking for.', 'uikit') ?></p>
            <p>
                <a href="<?php echo esc_url(home_url()); ?>"><i class="uk-icon-reply"></i> <?php echo __('Go back to homepage', 'uikit') ?></a>
            </p>
        </div>
    </section>
<?php
get_template_part('elements/base/postcontent');
get_template_part('elements/base/footer'); ?>
<?php get_footer();
