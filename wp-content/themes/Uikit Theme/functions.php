<?php

require('php/Theme.php');

/**
 * Theme setup function
 */
/*Add any custom styles or js to this function*/


function wpb_adding_scripts()
{
wp_enqueue_style( 'main', get_stylesheet_directory_uri()."/css/main.css" );
wp_enqueue_style( 'extra', get_template_directory_uri()."/css/extra.css" );
wp_enqueue_style( 'sidebar', get_stylesheet_directory_uri()."/sidebar.css" );
wp_enqueue_style( 'style', get_stylesheet_directory_uri()."/style.css" );
if (class_exists('woocommerce')){
    wp_enqueue_style( 'customwoocommerce', get_template_directory_uri()."/woocommerce/woocommerce.css" );
    }
wp_enqueue_script( 'uikitjs', get_template_directory_uri() . '/js/all.min.js', array(), '1.0.0', true );
if (mytheme_option('enablezoom')) {
wp_enqueue_script( 'elevatezoom', get_template_directory_uri() . '/js/jquery.elevatezoom.js', array('jquery'), '1.0.0', true );
wp_localize_script('elevatezoom', 'elevatezoom_vars', array(
			'mobile' => __((wp_is_mobile()), 'uikit'),
		));
}
if ( is_page_template( 'browserheight.php' ) ) {
    if (is_front_page()) { $home = '100'; } else { $home = '0'; }
wp_enqueue_script( 'stickyfooter', get_template_directory_uri() . '/js/stickyfooter.js', array('jquery'), '1.0.0', true );
wp_localize_script('stickyfooter', 'stickyfooter_vars', array(
			'mobile' => __(mytheme_option('mobiledropdown'), 'uikit'),
			'home'   => __($home, 'uikit'),
			'socialmediamob' => __(mytheme_option('socialmediamob'), 'uikit'),
		));}
};

add_action( 'wp_enqueue_scripts', 'wpb_adding_scripts' );



/*load uikit theme*/
if (!function_exists('wp_uikit_setup')) {
    function wp_uikit_setup()
    {
        global $theme;
        $theme = new Theme();
    }

    add_action('after_setup_theme', 'wp_uikit_setup');
}

function uikit_load_functions()
{
if (class_exists('woocommerce')){
   locate_template( array( 'woocommerce/woocommerce-functions.php' ), true, true );
    }
   locate_template( array( 'php/custom-functions.php' ), true, true );
   locate_template( array( 'php/UikitCustomizer.php' ), true, true );
}
add_action( 'after_setup_theme', 'uikit_load_functions' );


/*load theme options*/
if ( file_exists( get_template_directory() . '/class.my-theme-options.php' ) ) {
	require_once( get_template_directory() . '/class.my-theme-options.php' );
}

/*Grab custom max width from theme options*/

add_action('wp_head','hook_width');

function hook_width() {

  echo '<style> .uk-container { max-width: 100%; box-sizing: border-box;  padding: 0 25px;} @media (min-width: 1220px) {  .uk-container {  max-width: 100%; padding: 0 35px;  }} .uk-custom-width { min-height: 100%; position: relative; max-width: ';
	echo mytheme_option( 'Custom_Width' );
	echo 'px; margin: auto; background-color: ';
	echo mytheme_option( 'custombgcol') . '!important; box-shadow: 0px 0px 32px 0px rgba(0,0,0,1); }';
	echo '.uk-content-width {  max-width: ';
	echo mytheme_option( 'Custom_Width' );
	echo 'px; margin: 0 auto;}';
	echo '</style>';

}

add_action('wp_head','hook_fsbg');

function hook_fsbg() {

	echo '<style> .uk-custom-bg { min-height: 100%; position: relative; background-color: ';
	echo mytheme_option( 'custombgcol' );
	echo ' !important;}</style>';

}

add_action('wp_head','hook_mobile');

function hook_mobile() {

	echo '<style>.uk-flex-social { padding-left:5px } .uk-width-onesidebarcontent { width: 80%; } .uk-width-onesidebar { width: 20%; } .uk-width-twosidebar { width: 20%; } .uk-width-twosidebarcontent { width: 60%; } .nodisplay { display:block !important; } @media screen and (max-width:1200px) {.uk-width-onesidebarcontent { width: 75%; } .uk-width-onesidebar { width: 25%; } .uk-width-twosidebar { width: 20%; } .uk-width-twosidebarcontent { width: 60%; }} @media screen and (max-width: ';
	echo mytheme_option( 'mobiledropdown' );
	echo 'px) { .sticky-header-withsocialmob {  top: 40px !important; } .admin-bar .sticky-header-withsocialmob {  top: 86px !important; } .nodisplay { display:none !important; } } .mobile { display:none !important; } @media screen and (max-width:';
	echo mytheme_option( 'mobiledropdown' );
	echo 'px) { .mobile { display:block !important; } .uk-flex-social {  -ms-flex-pack: center;  -webkit-justify-content: center;  justify-content: center; padding-left:0px } .showlastleftsidebar{ order:98; } .showlastrightsidebar{ order:99; } .centersidebar { text-align:center !important; } .uk-width-onesidebarcontent { width: 100%; } .uk-width-onesidebar { width: 100%; } .uk-width-twosidebar { width: 100%; } .uk-width-twosidebarcontent { width: 100%; } }';
  echo '@media screen and (max-width:';
	echo mytheme_option( 'mobiledropdown' );
	echo 'px) {.uk-sticky-nav .uk-sticky-logo { display:none !important; }}';
	echo '</style>';


}

add_action('wp_head','hook_footer');

function hook_footer() {
    if (( is_page_template( 'browserheight.php' ) ) || ( is_page_template( 'fixedfooterdynamicpage.php' ) )) {
    echo '<style>html,body {height: 100%;  min-height: 100%;} </style>';
} else { echo '<style>html,body {min-height: 100%;} </style>';
}
}

add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

function UKisSecure() {
  return
    (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off')
    || $_SERVER['SERVER_PORT'] == 443;
}

if (mytheme_option('enableloading')) {

add_action('wp_head','hook_loading');

function hook_loading() {

	echo '<style>';
	/* Paste this css to your style sheet file or under head tag */
    /* This only works with JavaScript,
        if it's not present, don't show loader */
        echo '.no-js #loader { display: none;  }';
        echo '.js #loader { display: block; position: absolute; left: 100px; top: 0; }';
        echo '.se-pre-con {';
        	echo 'position: fixed;';
        	echo 'left: 0px;';
        	echo 'top: 0px;';
        	echo 'width: 100%;';
        	echo 'height: 100%;';
        	echo 'z-index: 9999;';
        	echo 'background: url(';
        	    echo mytheme_option('loadingicon');
        	    echo ') center no-repeat #fff;}';

	echo '</style>';
        if (UKisSecure()) {
          $protocol = 'https';
        } else {
          $protocol = 'http';
        }
    echo '<script src="'.$protocol.'://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>';


	echo '<script>jQuery(window).load(function() {';
	echo '	jQuery(".se-pre-con").fadeOut("slow");;';
	echo '});';
    echo '</script>';
}
}

/*Load in custom css from the theme options*/
add_action('wp_head','hook_css');

function hook_css() {

	echo '<style>';
  echo '#TopScroll { background-color:';
  echo mytheme_option( 'scrollcolour' );
  echo    ';}';
	echo mytheme_option( 'custom_css' );
	echo '</style>';

}

if (mytheme_option('scrolltotop')) {
  add_action('wp_head','hook_stt');

  function hook_stt() {
    if (trim(mytheme_option( 'scrollcolour' )) != '') {
  	echo '<style>';
    echo '#TopScroll { background-color:';
    echo mytheme_option( 'scrollcolour' );
    echo    ';}';
  	echo '</style>'; } ?>
    <script>
    // Scroll to top
    // When the user scrolls down 20px from the top of the document, show the button
    window.onscroll = function() {scrollFunction()};

    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            document.getElementById("TopScroll").style.display = "block";
        } else {
            document.getElementById("TopScroll").style.display = "none";
        }
    }

    // When the user clicks on the button, scroll to the top of the document
    function topFunction() {
        document.body.scrollTop = 0; // For Chrome, Safari and Opera
        document.documentElement.scrollTop = 0; // For IE and Firefox
    }
    </script>
    <?php

  }
}

/* Changes posts to products */
/*
function revcon_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Products';
    $submenu['edit.php'][5][0] = 'Products';
    $submenu['edit.php'][10][0] = 'Add Products';
    $submenu['edit.php'][16][0] = 'Products Tags';
    echo '';
}
function revcon_change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Products';
    $labels->singular_name = 'Products';
    $labels->add_new = 'Add Products';
    $labels->add_new_item = 'Add Products';
    $labels->edit_item = 'Edit Products';
    $labels->new_item = 'Products';
    $labels->view_item = 'View Products';
    $labels->search_items = 'Search Products';
    $labels->not_found = 'No Products found';
    $labels->not_found_in_trash = 'No Products found in Trash';
    $labels->all_items = 'All Products';
    $labels->menu_name = 'Products';
    $labels->name_admin_bar = 'Products';
}

add_action( 'admin_menu', 'revcon_change_post_label' );
add_action( 'init', 'revcon_change_post_object' ); */

// To give Editors access to the ALL Forms menu
function uikittheme_ninja_forms_all_forms_capabilities_filter( $capabilities ) {
    $capabilities = "edit_pages";
    return $capabilities;
}
add_filter( 'ninja_forms_admin_parent_menu_capabilities', 'uikittheme_ninja_forms_all_forms_capabilities_filter' );
add_filter( 'ninja_forms_admin_all_forms_capabilities', 'uikittheme_ninja_forms_all_forms_capabilities_filter' );
// To give Editors access to ADD New Forms
function uikittheme_ninja_forms_add_new_capabilities_filter( $capabilities ) {
    $capabilities = "edit_pages";
    return $capabilities;
}
add_filter( 'ninja_forms_admin_parent_menu_capabilities', 'uikittheme_ninja_forms_add_new_capabilities_filter' );
add_filter( 'ninja_forms_admin_add_new_capabilities', 'uikittheme_ninja_forms_add_new_capabilities_filter' );
/* To give Editors access to the Submissions - Simply replace �edit_posts� in the code snippet below with the capability
that you would like to attach the ability to view/edit submissions to.Please note that all three filters are needed to
provide proper submission viewing/editing on the backend!
*/
function nf_subs_capabilities( $cap ) {
    return 'edit_posts';
}
add_filter( 'ninja_forms_admin_submissions_capabilities', 'nf_subs_capabilities' );
add_filter( 'ninja_forms_admin_parent_menu_capabilities', 'nf_subs_capabilities' );
add_filter( 'ninja_forms_admin_menu_capabilities', 'nf_subs_capabilities' );
// To give Editors access to the Inport/Export Options
function uikittheme_ninja_forms_import_export_capabilities_filter( $capabilities ) {
    $capabilities = "edit_pages";
    return $capabilities;
}
add_filter( 'ninja_forms_admin_parent_menu_capabilities', 'uikittheme_ninja_forms_import_export_capabilities_filter' );
add_filter( 'ninja_forms_admin_import_export_capabilities', 'uikittheme_ninja_forms_import_export_capabilities_filter' );
// To give Editors access to the the Settings page
function uikittheme_ninja_forms_settings_capabilities_filter( $capabilities ) {
    $capabilities = "edit_pages";
    return $capabilities;
}
add_filter( 'ninja_forms_admin_parent_menu_capabilities', 'uikittheme_ninja_forms_settings_capabilities_filter' );
add_filter( 'ninja_forms_admin_settings_capabilities', 'uikittheme_ninja_forms_settings_capabilities_filter' );

/* Breadcrumbs */
function the_breadcrumb() {
	if (!is_home()) {
		echo '<a href="';
		echo get_option('home');
		echo '">';
		echo "<i class='uk-icon-home'></i></a> &#187; ";
		if (is_category() || is_single()) {
			foreach((get_the_category()) as $category) {
			echo $category->cat_name . ' ';
			}
			if ($category->cat_name == "") {
				$post_type = get_post_type_object( get_post_type($post) );
				echo $post_type->labels->name ;
				}
			if (is_single()) {
				echo " &#187; ";
				the_title();
			}
		}
		elseif (is_page()) {
			echo the_title();
		}
		elseif (is_tag()) {
			echo single_tag_title();
		}
		elseif (is_day()) {
			echo "Archive for ";
			the_time('F jS, Y');
		}
		elseif (is_month()) {
			echo "Archive for ";
			the_time('F, Y');
		}
		elseif (is_year()) {
			echo "Archive for ";
			the_time('Y');
		}
		elseif (is_author()) {
			echo "Author Archive";
		}
		elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {
		echo "Blog Archives";
		}
		elseif (is_search()) {
		echo "Search Results";
		}
	}
}

/**
 * Add all Gravity Forms capabilities to Editor role.
 * Runs when this theme is activated.
 *
 * @access public
 * @return void
 */
function grant_gforms_editor_access() {

  $role = get_role( 'editor' );
  $role->add_cap( 'gform_full_access' );
}
// Tie into the 'after_switch_theme' hook
add_action( 'after_switch_theme', 'grant_gforms_editor_access' );

/**
 * Remove Gravity Forms capabilities from Editor role.
 * Runs when this theme is deactivated (in favor of another).
 *
 * @access public
 * @return void
 */
function revoke_gforms_editor_access() {

  $role = get_role( 'editor' );
  $role->remove_cap( 'gform_full_access' );
}
// Tie into the 'switch_theme' hook
add_action( 'switch_theme', 'revoke_gforms_editor_access' );

?>
