<?php
/**
 * The main template file
 */

get_header();
get_template_part('elements/base/header');
get_template_part('elements/base/navigation');

get_template_part('elements/base/precontent');

?> <div class="uk-container uk-margin-bottom uk-margin-top"> <?php

if (have_posts()) {
    while (have_posts()) {
        the_post();
        get_template_part('elements/content/content'/*, get_post_format()*/);
    }

    // Previous/next page navigation.
    echo $theme->helpers->getPostsPagination();
} else {
    echo '<p>' . __('Nothing found here. Sorry!', 'uikit') . '</p>';
}

get_template_part('elements/base/postcontent');

get_template_part('elements/base/footer');

?> </div> <?php

get_footer();
