<?php
/**
 * Default template for sidebar
 */
?>

<?php if (is_active_sidebar('sidebar-secondaryshop')) : ?>
    <div id="shop-sidebar-right" class="widget-area" role="complementary" style="padding:10px;">        
        <?php dynamic_sidebar('sidebar-secondaryshop'); ?>
    </div>
<?php endif; ?>
